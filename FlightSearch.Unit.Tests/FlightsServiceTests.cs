using AutoMapper;
using FlightSearch.Common.Settings;
using FlightSearch.DataAccess.Airports;
using FlightSearch.DataAccess.Flights;
using FlightSearch.DataAccess.Models;
using FlightSearch.Service.Days;
using FlightSearch.Service.FlightDays;
using FlightSearch.Service.Flights;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Xunit;

namespace FlightSearch.Unit.Tests
{
    public class FlightsServiceTests
    {
        private readonly Mock<IOptions<AppSettings>> _appSettingsMock; // name should contain mock
        private readonly IMapper _mapper;
        private readonly Mock<IFlightsDataAccess> _flightsDataAccessMock;
        private readonly Mock<IAirportsDataAccess> _airportsDataAccessMock;
        private AppSettings _appSettings;
        private readonly FlightsService _flightsService;

        public FlightsServiceTests()
        {
            var config = new MapperConfiguration(opts => opts.AddMaps(
                new List<Assembly>
                {
                    typeof(FlightsMappingProfile).Assembly
                }));
            _mapper = config.CreateMapper();
            _appSettingsMock = new Mock<IOptions<AppSettings>>();
            _flightsDataAccessMock = new Mock<IFlightsDataAccess>();
            _airportsDataAccessMock = new Mock<IAirportsDataAccess>();
            _appSettings = new AppSettings
            {
                NoFlight = "The flight does not exist.",
                NoOrigin = "The origin airport does not exist.",
                NoDestination = "The destination airport does not exist.",
                FoundCode = "Flight with that code already exists."
            };
            _appSettingsMock.SetupGet(x => x.Value).Returns(_appSettings);
            _flightsService = new FlightsService(
                _flightsDataAccessMock.Object, // object from mock is passed as parameter
                _mapper,
                _airportsDataAccessMock.Object,
                _appSettingsMock.Object
                );
            
        }

        private FlightDay GetFlightDay(int id)
        {
            FlightDay flightDay = new FlightDay();
            flightDay.Day = new Day();

            switch (id)
            {
                case 1:
                    flightDay.DayId = id;
                    flightDay.Day.DayId = id;
                    flightDay.Day.Name = "pon";
                    break;
                case 2:
                    flightDay.DayId = id;
                    flightDay.Day.DayId = id;
                    flightDay.Day.Name = "uto";
                    break;
                case 3:
                    flightDay.DayId = id;
                    flightDay.Day.DayId = id;
                    flightDay.Day.Name = "sre";
                    break;
                case 4:
                    flightDay.DayId = id;
                    flightDay.Day.DayId = id;
                    flightDay.Day.Name = "cet";
                    break;
                case 5:
                    flightDay.DayId = id;
                    flightDay.Day.DayId = id;
                    flightDay.Day.Name = "pet";
                    break;
                case 6:
                    flightDay.DayId = id;
                    flightDay.Day.DayId = id;
                    flightDay.Day.Name = "sub";
                    break;
                case 7:
                    flightDay.DayId = id;
                    flightDay.Day.DayId = id;
                    flightDay.Day.Name = "ned";
                    break;
                default:
                    break;
            }
            return flightDay;

        }

        private DayDomain GetDayDomain(int id)
        {
            DayDomain flightDay = new DayDomain();
            
            switch (id)
            {
                case 1:
                    flightDay.DayId = id;
                    flightDay.Name = "pon";
                    break;
                case 2:
                    flightDay.DayId = id;
                    flightDay.Name = "uto";
                    break;
                case 3:
                    flightDay.DayId = id;
                    flightDay.Name = "sre";
                    break;
                case 4:
                    flightDay.DayId = id;
                    flightDay.Name = "cet";
                    break;
                case 5:
                    flightDay.DayId = id;
                    flightDay.Name = "pet";
                    break;
                case 6:
                    flightDay.DayId = id;
                    flightDay.Name = "sub";
                    break;
                case 7:
                    flightDay.DayId = id;
                    flightDay.Name = "ned";
                    break;
                default:
                    break;
            }
            return flightDay;

        }

        [Fact]
        public async Task GetById_FlightExists_ReturnsFlightDomain()
        {
            // Arrange            
            int flightId = 1;

            var flight = new Flight
            {
                FlightId = flightId,
                Code = "text",
                OriginAirportId = 1,
                DestinationAirportId = 2,
                Agency = "Agency1",
                DepartureTime ="12:30",
                LandingTime = "15:00",
                Columns = 12,
                Rows = 13,
                Price = 200,
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(3),
                    GetFlightDay(7),
                }
            };

            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(flightId)).ReturnsAsync(flight);

            // Act
            var foundFlight = await _flightsService.GetByIdAsync(flightId);

            // Assert
            Assert.NotNull(foundFlight);
            Assert.Equal(flightId, foundFlight.Id);
            Assert.Equal("text", foundFlight.Code);
            Assert.Equal(1, foundFlight.OriginAirportId);
            Assert.Equal(2, foundFlight.DestinationAirportId);
            Assert.Equal("Agency1", foundFlight.Agency);
            Assert.Equal("12:30", foundFlight.DepartureTime);
            Assert.Equal("15:00", foundFlight.LandingTime);
            Assert.Equal(12, foundFlight.Columns);
            Assert.Equal(13, foundFlight.Rows);
            Assert.Equal(200, foundFlight.Price);
            Assert.Equal(2, foundFlight.Days.Count);
            Assert.Equal(3, foundFlight.Days[0]);
            Assert.Equal(7, foundFlight.Days[1]);
        }

        [Fact]
        public async Task GetById_FlightNotExists_ReturnsFlightDomain()
        {
            // Arrange
            int flightId = 1;

            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(flightId)).ReturnsAsync((Flight)null);

            // Act
            var foundFlight = await _flightsService.GetByIdAsync(flightId);
             
            // Assert
            Assert.Null(foundFlight);
        }

        [Fact]
        public async Task EditAsync_AllPropertiesExceptDays_ReturnsFlightUpdateDomain()
        {
            // Arrange
            int flightId = 1;
            int originAirportId = 3;
            int destinationAirportId = 4;

            var flight = new Flight
            {
                FlightId = flightId,
                Code = "text",
                OriginAirportId = 1,
                DestinationAirportId = 2,
                Agency = "Agency1",
                DepartureTime = "12:30",
                LandingTime = "15:00",
                Columns = 12,
                Rows = 13,
                Price = 200,
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(3),
                    GetFlightDay(7)
                }
            };

            var originAirport = new Airport
            {
                AirportId = originAirportId
            };

            var destinationAirport = new Airport
            {
                AirportId = destinationAirportId
            };

            var flightDomain = new FlightDomain
            {
                Id = flightId,
                Code = "text1",
                OriginAirportId = 3,
                DestinationAirportId = 4,
                Agency = "Agency2",
                DepartureTime = "11:15",
                LandingTime = "13:30",
                Columns = 11,
                Rows = 20,
                Price = 150,
                Days = null,
            };

            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(flightId)).ReturnsAsync(flight);
            _airportsDataAccessMock.Setup(x => x.FindById(originAirportId)).ReturnsAsync(originAirport);
            _airportsDataAccessMock.Setup(x => x.FindById(destinationAirportId)).ReturnsAsync(destinationAirport);
            _flightsDataAccessMock.Setup(x => x.RemoveFlightDaysAsync(flightId));

            // Act
            var updatedFlight = await _flightsService.EditAsync(flightId, flightDomain);

             // Assert
            Assert.NotNull(updatedFlight);
            Assert.Equal(1, flightId);
            Assert.Equal(1, updatedFlight.Id);
            Assert.Equal("text1", updatedFlight.Code);
            Assert.Equal(3, updatedFlight.OriginAirportId);
            Assert.Equal(4, updatedFlight.DestinationAirportId);
            Assert.Equal("Agency2", updatedFlight.Agency);
            Assert.Equal("11:15", updatedFlight.DepartureTime);
            Assert.Equal("13:30", updatedFlight.LandingTime);
            Assert.Equal(11, updatedFlight.Columns);
            Assert.Equal(20, updatedFlight.Rows);
            Assert.Equal(150, updatedFlight.Price);
            Assert.Equal(2, updatedFlight.FlightDays.Count);
            _flightsDataAccessMock.Verify(x => x.RemoveFlightDaysAsync(flightId), Times.Never());
            Assert.Equal("sre", updatedFlight.FlightDays[0].Day.Name);
            Assert.Equal("ned", updatedFlight.FlightDays[1].Day.Name);
            
        }

        [Fact]
        public async Task EditAsync_NoPropertiesExceptId_ReturnsFlightUpdateDomain()
        {
            // Arrange
            int flightId = 1;
           
            var flight = new Flight
            {
                FlightId = flightId,
                Code = "text",
                OriginAirportId = 1,
                DestinationAirportId = 2,
                Agency = "Agency1",
                DepartureTime = "12:30",
                LandingTime = "15:00",
                Columns = 12,
                Rows = 13,
                Price = 200,
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(3),
                    GetFlightDay(7)
                }
            };

            var flightDomain = new FlightDomain
            {
                Id = flightId,
            };

            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(flightId)).ReturnsAsync(flight);
            _flightsDataAccessMock.Setup(x => x.RemoveFlightDaysAsync(flightId));

            // Act
            var updatedFlight = await _flightsService.EditAsync(flightId, flightDomain);

            // Assert
            Assert.NotNull(updatedFlight);
            Assert.Equal(1, flightId);
            Assert.Equal(1, updatedFlight.Id);
            Assert.Equal("text", updatedFlight.Code);
            Assert.Equal(1, updatedFlight.OriginAirportId);
            Assert.Equal(2, updatedFlight.DestinationAirportId);
            Assert.Equal("Agency1", updatedFlight.Agency);
            Assert.Equal("12:30", updatedFlight.DepartureTime);
            Assert.Equal("15:00", updatedFlight.LandingTime);
            Assert.Equal(12, updatedFlight.Columns);
            Assert.Equal(13, updatedFlight.Rows);
            Assert.Equal(200, updatedFlight.Price);
            Assert.Equal(2, updatedFlight.FlightDays.Count);
            _flightsDataAccessMock.Verify(x => x.RemoveFlightDaysAsync(flightId), Times.Never());
            Assert.Equal("sre", updatedFlight.FlightDays[0].Day.Name);
            Assert.Equal("ned", updatedFlight.FlightDays[1].Day.Name);
        }

        [Fact]
        public async Task EditAsync_FlightNotExist_ReturnsFlightUpdateDomain()
        {
            // Arrange
            int flightId = 1;
           
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(flightId)).ReturnsAsync((Flight)null);
           
            // Act
            var ex = await Assert.ThrowsAsync<Exception>(async () => await _flightsService.EditAsync(flightId, (FlightDomain)null));

            // Assert
            Assert.Equal(_appSettings.NoFlight, ex.Message);
        }

        [Fact]
        public async Task EditAsync_FlightFoundByCode_ReturnsFlightUpdateDomain()
        {
            // Arrange
            int flightId = 1;
            string code = "123";
            int originAirportId = 1;
            int destinationAirportId = 2;

            var flight = new Flight {
                FlightId = flightId,

            };
            var flightByCode = new Flight
            {
                Code = code,
            };

            var originAirport = new Airport
            {
                AirportId = originAirportId,
            };

            var destinationAirport = new Airport
            {
                AirportId = destinationAirportId,
            };

            FlightDomain flightDomain = new FlightDomain {
                Id = flightId,
                Code = code,
            };

            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(flightId)).ReturnsAsync(flight);
            _flightsDataAccessMock.Setup(x => x.FindByCodeAsync(code)).ReturnsAsync(flightByCode);
            _airportsDataAccessMock.Setup(x => x.FindById(originAirportId)).ReturnsAsync(originAirport);
            _airportsDataAccessMock.Setup(x => x.FindById(destinationAirportId)).ReturnsAsync(destinationAirport);

            // Act
            var ex = await Assert.ThrowsAsync<Exception>(async () => await _flightsService.EditAsync(flightId, flightDomain));

            // Assert
            Assert.Equal(_appSettings.FoundCode, ex.Message);
        }

        [Fact]
        public async Task EditAsync_AllProperties_ReturnsFlightUpdateDomain()
        {
            // Arrange
            
            int flightId = 1;
            int originAirportId = 3;
            int destinationAirportId = 4;

            
            var flight = new Flight
            {
                FlightId = flightId,
                Code = "text",
                OriginAirportId = 1,
                DestinationAirportId = 2,
                Agency = "Agency1",
                DepartureTime = "12:30",
                LandingTime = "15:00",
                Columns = 12,
                Rows = 13,
                Price = 200,
                FlightDays = new List<FlightDay>
                {
                     GetFlightDay(3),
                     GetFlightDay(7)
                }
            };

            var originAirport = new Airport
            {
                AirportId = originAirportId
            };

            var destinationAirport = new Airport
            {
                AirportId = destinationAirportId
            };

            var flightDomain = new FlightDomain
            {
                Id = flightId,
                Code = "text1",
                OriginAirportId = 3,
                DestinationAirportId = 4,
                Agency = "Agency2",
                DepartureTime = "11:15",
                LandingTime = "13:30",
                Columns = 11,
                Rows = 20,
                Price = 150,
                Days = new List<int> { 2, 5 },
            };

            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(flightId)).ReturnsAsync(flight);
            _flightsDataAccessMock.Setup(x => x.AddFlightDaysToFlightAsync(flight.FlightId, flightDomain.Days));
            _flightsDataAccessMock.Setup(x => x.RemoveFlightDaysAsync(flightId));
            _flightsDataAccessMock.Setup(x => x.AddFlightDaysToFlightAsync(flightId, flightDomain.Days));
            _airportsDataAccessMock.Setup(x => x.FindById(originAirport.AirportId)).ReturnsAsync(originAirport);
            _airportsDataAccessMock.Setup(x => x.FindById(destinationAirport.AirportId)).ReturnsAsync(destinationAirport);
            
            // Act
            var updatedFlightDomain = await _flightsService.EditAsync(flightId, flightDomain);

            // Assert
            Assert.NotNull(updatedFlightDomain);
            Assert.Equal(1, flightId);
            Assert.Equal(1, updatedFlightDomain.Id);
            Assert.Equal("text1", updatedFlightDomain.Code);
            Assert.Equal(3, updatedFlightDomain.OriginAirportId);
            Assert.Equal(4, updatedFlightDomain.DestinationAirportId);
            Assert.Equal("Agency2", updatedFlightDomain.Agency);
            Assert.Equal("11:15", updatedFlightDomain.DepartureTime);
            Assert.Equal("13:30", updatedFlightDomain.LandingTime);
            Assert.Equal(11, updatedFlightDomain.Columns);
            Assert.Equal(20, updatedFlightDomain.Rows);
            Assert.Equal(150, updatedFlightDomain.Price);
            Assert.Equal(2, updatedFlightDomain.FlightDays.Count);
            _flightsDataAccessMock.Verify(x => x.RemoveFlightDaysAsync(flightId), Times.Once());
            _flightsDataAccessMock.Verify(x => x.AddFlightDaysToFlightAsync(flightId, flightDomain.Days), Times.Once());
            Assert.Equal("sre", updatedFlightDomain.FlightDays[0].Day.Name);
            Assert.Equal("ned", updatedFlightDomain.FlightDays[1].Day.Name);
        }

        [Fact]
        public async Task CreateAsync_OriginAirportNull_ReturnsFLightUpdateDomain()
        {
            // Arrange
            
            int flightId = 1;
            int originAirpotId = 1;
            
            Airport originAirport = null;
            FlightDomain flightDomain = new FlightDomain {
                Id = flightId,
                Code = "text1",
                OriginAirportId = originAirpotId,
                DestinationAirportId = 4,
                Agency = "Agency2",
                DepartureTime = "11:15",
                LandingTime = "13:30",
                Columns = 11,
                Rows = 20,
                Price = 150,        
            };
           
            _airportsDataAccessMock.Setup(x => x.FindById(flightDomain.OriginAirportId)).ReturnsAsync(originAirport);

            // Act
            var ex = await Assert.ThrowsAsync<Exception>(async () => await _flightsService.CreateAsync(flightDomain));

            // Assert
            Assert.Equal(_appSettings.NoOrigin, ex.Message);
        }

        [Fact]
        public async Task CreateAsync_DestinationAirportNull_ReturnsFLightUpdateDomain()
        {
            // Arrange
            int flightId = 1;
            int destinationAirportId = 1;

            FlightDomain flightDomain = new FlightDomain
            {
                Id = flightId,
                Code = "text1",
                OriginAirportId = 2,
                DestinationAirportId = destinationAirportId,
                Agency = "Agency2",
                DepartureTime = "11:15",
                LandingTime = "13:30",
                Columns = 11,
                Rows = 20,
                Price = 150,
            };

            _airportsDataAccessMock.Setup(x => x.FindById(flightDomain.DestinationAirportId)).ReturnsAsync((Airport) null);

            // Act
            var ex = await Assert.ThrowsAsync<Exception>(async () => await _flightsService.CreateAsync(flightDomain));

            // Assert
            Assert.Equal(_appSettings.NoOrigin, ex.Message);
        }

        [Fact]
        public async Task CreateAsync_AllPropertiesExceptDays_ReturnsFLightUpdateDomain()
        {
            // Arrange
            int flightId = 1;
            int originAirportId = 3;
            int destinationAirportId = 4;

            var flight = new Flight
            {
                FlightId = flightId,
                Code = "text1",
                OriginAirportId = 3,
                DestinationAirportId = 4,
                Agency = "Agency2",
                DepartureTime = "11:15",
                LandingTime = "13:30",
                Columns = 11,
                Rows = 20,
                Price = 150,
            };

            var newFlight = new Flight
            {
                FlightId = flightId,
                Code = "text1",
                OriginAirportId = 3,
                DestinationAirportId = 4,
                Agency = "Agency2",
                DepartureTime = "11:15",
                LandingTime = "13:30",
                Columns = 11,
                Rows = 20,
                Price = 150,
            };

            var flightDomain = new FlightDomain
            {
                Id = flightId,
                Code = "text1",
                OriginAirportId = 3,
                DestinationAirportId = 4,
                Agency = "Agency2",
                DepartureTime = "11:15",
                LandingTime = "13:30",
                Columns = 11,
                Rows = 20,
                Price = 150,
                Days = new List<int> { },
            };

            var originAirport = new Airport
            {
                AirportId = originAirportId
            };

            var destinationAirport = new Airport
            {
                AirportId = destinationAirportId
            };

            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(flight.FlightId)).ReturnsAsync(flight);
            _airportsDataAccessMock.Setup(x => x.FindById(originAirport.AirportId)).ReturnsAsync(originAirport);
            _airportsDataAccessMock.Setup(x => x.FindById(destinationAirport.AirportId)).ReturnsAsync(destinationAirport);
            _flightsDataAccessMock.Setup(x => x.FindByCodeAsync(flight.Code)).ReturnsAsync((Flight)null);
            _flightsDataAccessMock.Setup(x => x.InsertAsync(It.IsAny<Flight>())).ReturnsAsync(newFlight);
            _flightsDataAccessMock.Verify(x => x.AddFlightDaysToFlightAsync(flightId, flightDomain.Days), Times.Never());

            // Act
            var updatedFlight = await _flightsService.CreateAsync(flightDomain);

            // Assert
            Assert.NotNull(updatedFlight);
            Assert.Equal(1, updatedFlight.Id);
            Assert.Equal("text1", updatedFlight.Code);
            Assert.Equal(3, updatedFlight.OriginAirportId);
            Assert.Equal(4, updatedFlight.DestinationAirportId);
            Assert.Equal("Agency2", updatedFlight.Agency);
            Assert.Equal("11:15", updatedFlight.DepartureTime);
            Assert.Equal("13:30", updatedFlight.LandingTime);
            Assert.Equal(11, updatedFlight.Columns);
            Assert.Equal(20, updatedFlight.Rows);
            Assert.Equal(150, updatedFlight.Price);
            _flightsDataAccessMock.Verify(x => x.InsertAsync(It.IsAny<Flight>()), Times.Once());
            _flightsDataAccessMock.Verify(x => x.AddFlightDaysToFlightAsync(flightId, flightDomain.Days), Times.Never());
        }

        [Fact]
        public async Task CreateAsync_AllProperties_ReturnsFLightUpdateDomain()
        {
            // Arrange  
            int flightId = 1;
            int originAirportId = 3;
            int destinationAirportId = 4;

            var flight = new Flight
            {
                FlightId = flightId,
                Code = "text1",
                OriginAirportId = 3,
                DestinationAirportId = 4,
                Agency = "Agency2",
                DepartureTime = "11:15",
                LandingTime = "13:30",
                Columns = 11,
                Rows = 20,
                Price = 150,
            };

            var newFlight = new Flight
            {
                FlightId = flightId,
                Code = "text1",
                OriginAirportId = 3,
                DestinationAirportId = 4,
                Agency = "Agency2",
                DepartureTime = "11:15",
                LandingTime = "13:30",
                Columns = 11,
                Rows = 20,
                Price = 150,
            };

            var flightDomain = new FlightDomain
            {
                Id = flightId,
                Code = "text1",
                OriginAirportId = 3,
                DestinationAirportId = 4,
                Agency = "Agency2",
                DepartureTime = "11:15",
                LandingTime = "13:30",
                Columns = 11,
                Rows = 20,
                Price = 150,
                Days = new List<int> {1, 2 },
            };

            var originAirport = new Airport
            {
                AirportId = originAirportId
            };

            var destinationAirport = new Airport
            {
                AirportId = destinationAirportId
            };

            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(flight.FlightId)).ReturnsAsync(flight);
            _airportsDataAccessMock.Setup(x => x.FindById(originAirport.AirportId)).ReturnsAsync(originAirport);
            _airportsDataAccessMock.Setup(x => x.FindById(destinationAirport.AirportId)).ReturnsAsync(destinationAirport);
            _flightsDataAccessMock.Setup(x => x.FindByCodeAsync(flight.Code)).ReturnsAsync((Flight)null);
            _flightsDataAccessMock.Setup(x => x.InsertAsync(It.IsAny<Flight>())).ReturnsAsync(newFlight);
            _flightsDataAccessMock.Setup(x => x.AddFlightDaysToFlightAsync(flightId, flightDomain.Days));

            // Act
            var updatedFlight = await _flightsService.CreateAsync(flightDomain);

            // Assert
            Assert.NotNull(updatedFlight);
            Assert.Equal(1, updatedFlight.Id);
            Assert.Equal("text1", updatedFlight.Code);
            Assert.Equal(3, updatedFlight.OriginAirportId);
            Assert.Equal(4, updatedFlight.DestinationAirportId);
            Assert.Equal("Agency2", updatedFlight.Agency);
            Assert.Equal("11:15", updatedFlight.DepartureTime);
            Assert.Equal("13:30", updatedFlight.LandingTime);
            Assert.Equal(11, updatedFlight.Columns);
            Assert.Equal(20, updatedFlight.Rows);
            Assert.Equal(150, updatedFlight.Price);
            _flightsDataAccessMock.Verify(x => x.InsertAsync(It.IsAny<Flight>()), Times.Once());
            _flightsDataAccessMock.Verify(x => x.AddFlightDaysToFlightAsync(flightId, flightDomain.Days), Times.Once());
        }

        [Fact]
        public async Task CreateAsync_FlightFoundByCode_ReturnsFLightUpdateDomain()
        {
            // Arrange     
            int flightId = 1;
            string code = "123";
            int originAirportId = 1;
            int destinationAirportId = 2;

            var flightByCode = new Flight
            {
                Code = code,
            };

            var originAirport = new Airport
            {
                AirportId = originAirportId,
            };

            var destinationAirport = new Airport
            {
                AirportId = destinationAirportId,
            };

            FlightDomain flightDomain = new FlightDomain
            {
                Id = flightId,
                Code = code,
                OriginAirportId = originAirportId,
                DestinationAirportId = destinationAirportId

            };

            _airportsDataAccessMock.Setup(x => x.FindById(flightDomain.OriginAirportId)).ReturnsAsync(originAirport);
            _airportsDataAccessMock.Setup(x => x.FindById(flightDomain.DestinationAirportId)).ReturnsAsync(destinationAirport);
            _flightsDataAccessMock.Setup(x => x.FindByCodeAsync(code)).ReturnsAsync(flightByCode);

            // Act
            var ex = await Assert.ThrowsAsync<Exception>(async () => await _flightsService.CreateAsync(flightDomain));

            // Assert
            Assert.Equal(_appSettings.FoundCode, ex.Message);
        }

        [Fact]
        public async Task GetByOriginAndDestinationAsync_MoreConnectedFlightsExistButDirectNot_ReturnsFlightsCombination()
        {
            // Arrange  
            int originAirportId = 1;
            int firstConnectedAirportId = 2;
            int destinationAirportId = 3;
            int secondConnectedAirportId = 4;

            int firstFlightId = 1;
            int secondFlightId = 2;
            int thirdFlightId = 3;
            int forthFlightId = 4;
            int fifthFlightId = 5;
 
            var firstFlight = new Flight
            {
                FlightId = firstFlightId,
                OriginAirportId = originAirportId,
                DestinationAirportId = firstConnectedAirportId,
                DepartureTime = "7:00",
                LandingTime = "12:00",
                FlightDays = new List<FlightDay>
                {
                     GetFlightDay(3),
                     GetFlightDay(6)
                }
            };

            var secondFlight = new Flight
            {
                FlightId = secondFlightId,
                OriginAirportId = firstConnectedAirportId,
                DestinationAirportId = destinationAirportId,
                DepartureTime = "16:00",
                LandingTime = "18:00",
                FlightDays = new List<FlightDay>
                {
                     GetFlightDay(3),
                     GetFlightDay(7)
                }
            };

            var thirdFlight = new Flight
            {
                FlightId = thirdFlightId,
                OriginAirportId = originAirportId,
                DestinationAirportId = destinationAirportId,
                DepartureTime = "17:00",
                LandingTime = "18:00",
                FlightDays = new List<FlightDay>
                {
                     GetFlightDay(3),
                     GetFlightDay(7)
                }
            };

            var fourthFlight = new Flight
            {
                FlightId = forthFlightId,
                OriginAirportId = originAirportId,
                DestinationAirportId = secondConnectedAirportId,
                DepartureTime = "17:00",
                LandingTime = "18:00",
                FlightDays = new List<FlightDay>
                {
                     GetFlightDay(3),
                     GetFlightDay(7)
                }
            };

            var fifthFlight = new Flight
            {
                FlightId = fifthFlightId,
                OriginAirportId = secondConnectedAirportId,
                DestinationAirportId = destinationAirportId,
                DepartureTime = "20:00",
                LandingTime = "22:00",
                FlightDays = new List<FlightDay>
                {
                     GetFlightDay(3),
                     GetFlightDay(7)
                }
            };

            var originFlights = new List<Flight> {};
            originFlights.Add(firstFlight);
            originFlights.Add(fourthFlight);
            var destinationFlights = new List<Flight> { };
            destinationFlights.Add(secondFlight);
            destinationFlights.Add(fifthFlight);


            _flightsDataAccessMock.Setup(x => x.FindByOriginAndDestinationAsync(originAirportId, destinationAirportId)).ReturnsAsync(new List<Flight>());
            _flightsDataAccessMock.Setup(x => x.FindByOriginAsync(originAirportId)).ReturnsAsync(originFlights);
            _flightsDataAccessMock.Setup(x => x.FindByDestinationAsync(destinationAirportId)).ReturnsAsync(destinationFlights);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(firstFlightId)).ReturnsAsync(firstFlight);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(thirdFlightId)).ReturnsAsync(thirdFlight);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(secondFlightId)).ReturnsAsync(secondFlight);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(forthFlightId)).ReturnsAsync(fourthFlight);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(fifthFlightId)).ReturnsAsync(fifthFlight);

            // Act
            var flightsCombination = await _flightsService.GetByOriginAndDestinationAsync(originAirportId, destinationAirportId);

            // Assert
            Assert.Equal(2, flightsCombination.ConnectingFlights.Count);
            Assert.Empty(flightsCombination.DirectFlights);
            Assert.NotNull(flightsCombination.FeaturedFlight);
            Assert.Equal(4, flightsCombination.FeaturedFlight.FlightCombination.OriginFlight.Id);
            Assert.Equal(5, flightsCombination.FeaturedFlight.FlightCombination.DestinationFlight.Id);
            Assert.Equal(3, flightsCombination.FeaturedFlight.Day);
        }

        [Fact]
        public async Task GetByOriginAndDestinationAsync_OneConnectedFlightExistsButDirectNot_ReturnsFlightsCombination()
        {
            // Arrange            
            int firstAirportId = 1;
            int secondAirportId = 2;
            int thirdAirportId = 3;

            int firstFlightId = 1;
            int secondFlightId = 2;
            int thirdFlightId = 3;
                      
            var firstFlight = new Flight
            {
                FlightId = firstFlightId,
                OriginAirportId = firstAirportId,
                DestinationAirportId = secondAirportId,
                DepartureTime = "7:00",
                LandingTime = "12:00",
                FlightDays = new List<FlightDay>
                {
                     GetFlightDay(3),
                     GetFlightDay(6)
                }
            };

            var secondFlight = new Flight
            {
                FlightId = secondFlightId,
                OriginAirportId = secondAirportId,
                DestinationAirportId = thirdAirportId,
                DepartureTime = "16:00", 
                LandingTime = "18:00",
               FlightDays = new List<FlightDay>
                {
                     GetFlightDay(3),
                     GetFlightDay(7)
                }
            };

            var thirdFlight = new Flight
            {
                FlightId = thirdFlightId,
                OriginAirportId = firstAirportId,
                DestinationAirportId = thirdAirportId,
                DepartureTime = "17:00", 
                LandingTime = "18:00",
                FlightDays = new List<FlightDay>
                {
                     GetFlightDay(3),
                     GetFlightDay(7)
                }
            };

            var firstFlightDomain = new FlightUpdateDomain
            {
                Id = firstFlightId,
                OriginAirportId = firstAirportId,
                DestinationAirportId = secondAirportId,
                DepartureTime = "7:00",
                LandingTime = "12:00",
            };

            var secondFlightDomain = new FlightUpdateDomain
            {
                Id = secondFlightId,
                OriginAirportId = secondAirportId,
                DestinationAirportId = thirdAirportId,
                DepartureTime = "16:00",
                LandingTime = "18:00",
            };

            var thirdFlightDomain = new FlightUpdateDomain
            {
                Id = thirdFlightId,
                OriginAirportId = firstAirportId,
                DestinationAirportId = thirdAirportId,
                DepartureTime = "17:00",
                LandingTime = "18:00",
            };
            var connectedFlights = new List<ConnectingFlightsDomain>
            {
                new ConnectingFlightsDomain
                {
                    OriginFlight = firstFlightDomain,
                    DestinationFlight = thirdFlightDomain
                },
            };

           var flightsByOrigin = new List<Flight>();
            flightsByOrigin.Add(firstFlight);

            var flightsByDestination = new List<Flight>();
            flightsByDestination.Add(secondFlight);

            _flightsDataAccessMock.Setup(x => x.FindByOriginAndDestinationAsync(firstAirportId, thirdAirportId)).ReturnsAsync(new List<Flight>());
            _flightsDataAccessMock.Setup(x => x.FindByOriginAsync(firstAirportId)).ReturnsAsync(flightsByOrigin);
            _flightsDataAccessMock.Setup(x => x.FindByDestinationAsync(thirdAirportId)).ReturnsAsync(flightsByDestination);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(firstFlightId)).ReturnsAsync(firstFlight);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(thirdFlightId)).ReturnsAsync(thirdFlight);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(secondFlightId)).ReturnsAsync(secondFlight);

            // Act
            var flightsCombination = await _flightsService.GetByOriginAndDestinationAsync(firstAirportId, thirdAirportId);

            // Assert
            Assert.Equal(connectedFlights.Count, flightsCombination.ConnectingFlights.Count);
            Assert.Equal(connectedFlights[0].OriginFlight.Id, flightsCombination.ConnectingFlights[0].OriginFlight.Id);
            Assert.Empty(flightsCombination.DirectFlights);
            Assert.NotNull(flightsCombination.FeaturedFlight);
            Assert.Equal(1, flightsCombination.FeaturedFlight.FlightCombination.OriginFlight.Id);
            Assert.Equal(2, flightsCombination.FeaturedFlight.FlightCombination.DestinationFlight.Id);
            Assert.Equal(3, flightsCombination.FeaturedFlight.Day);
        }

        [Fact]
        public async Task GetByOriginAndDestinationAsync_DirectFlightsExistButConnectedNot_ReturnsFlightsCombination()
        {
            // Arrange           
            int firstFlightId = 1;
            int secondFlightId = 2;
            int originId = 1;
            int destinationId = 2;
        
            var firstFlight = new Flight
            {
                FlightId = firstFlightId,
            };

            var secondFlight = new Flight
            {
                FlightId = secondFlightId,
            };

            var directFlights = new List<Flight>();
            directFlights.Add(firstFlight);
            directFlights.Add(secondFlight);

            _flightsDataAccessMock.Setup(x => x.FindByOriginAndDestinationAsync(originId, destinationId)).ReturnsAsync(directFlights);
            _flightsDataAccessMock.Setup(x => x.FindByOriginAsync(originId)).ReturnsAsync(new List<Flight>());
            _flightsDataAccessMock.Setup(x => x.FindByDestinationAsync(destinationId)).ReturnsAsync(new List<Flight>());

            // Act
            var flightCombination = await _flightsService.GetByOriginAndDestinationAsync(originId, destinationId);
            
            // Assert
            Assert.Equal(directFlights.Count, flightCombination.DirectFlights.Count);
            Assert.Equal(1, flightCombination.DirectFlights[0].Id);
            Assert.Equal(2, flightCombination.DirectFlights[1].Id);
            Assert.Empty(flightCombination.ConnectingFlights);
            Assert.Null(flightCombination.FeaturedFlight);   
        }

        [Fact]
        public async Task GetFeaturedFlight_DaysAreSameButTimeDifferenceIsDifferent_ReturnsFlightsCombination()
        {
            // Arrange          
            var originAirtportId = 5;
            var destinationAirportId = 8;
            var commonAirportIdFirst = 11;
            var commonAirportIdSecond = 12;
            var originFlightIdFirst = 1;
            var destinationFlightIdFirst = 2;
            var originFlightIdSecond = 3;
            var destinationFlightIdSecond = 4;
            
            var originFlightFirst = new Flight
            {
                FlightId = originFlightIdFirst,
                DepartureTime = "7:00",
                LandingTime = "12:00",
                FlightDays = new List<FlightDay>
                {
                     GetFlightDay(2),
                     GetFlightDay(6)
                }
            };

            var destinationFlightFirst = new Flight
            {
                FlightId = destinationFlightIdFirst,
                DepartureTime = "16:00", // 4 hrs difference
                LandingTime = "18:00",
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(2), //same day
                    GetFlightDay(6)
                }
            };

            var originFlightSecond = new Flight
            {
                FlightId = originFlightIdSecond,
                DepartureTime = "6:00",
                LandingTime = "11:00",
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(2),
                    GetFlightDay(6)
                }
            };

            var destinationFlightSecond = new Flight
            {
                FlightId = destinationFlightIdSecond,
                DepartureTime = "14:00", // 3 hrs difference
                LandingTime = "17:00",
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(2), //same day
                    GetFlightDay(6)
                }
            };

            var originFlightDomainFirst = new FlightUpdateDomain
            {
                Id = originFlightIdFirst,
                OriginAirportId = originAirtportId,
                DestinationAirportId = commonAirportIdFirst,
                DepartureTime = "7:00",
                LandingTime = "12:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day = GetDayDomain(2) },
                    new FlightDayDomain { Day = GetDayDomain(6) }
                }
            };

            var destinationFlightDomainFirst = new FlightUpdateDomain
            {
                Id = destinationFlightIdFirst,
                OriginAirportId = commonAirportIdFirst,
                DestinationAirportId = destinationAirportId,
                DepartureTime = "16:00", // 4 hrs difference
                LandingTime = "18:00",
                FlightDays = new List<FlightDayDomain>
                {
                     new FlightDayDomain { Day = GetDayDomain(2) }, //same day
                     new FlightDayDomain { Day = GetDayDomain(6) }
                }
            };

            var originFlightDomainSecond = new FlightUpdateDomain
            {
                Id = originFlightIdSecond,
                OriginAirportId = originAirtportId,
                DestinationAirportId = commonAirportIdSecond,
                DepartureTime = "6:00",
                LandingTime = "11:00",
                FlightDays = new List<FlightDayDomain>
                {
                     new FlightDayDomain { Day = GetDayDomain(2) }, //same day
                     new FlightDayDomain { Day = GetDayDomain(6) }
                }
            };

            var destinationFlightDomainSecond = new FlightUpdateDomain
            {
                Id = destinationFlightIdSecond,
                OriginAirportId = commonAirportIdSecond,
                DestinationAirportId = destinationAirportId,
                DepartureTime = "14:00", // 3 hrs difference
                LandingTime = "17:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day = GetDayDomain(2) },
                    new FlightDayDomain { Day = GetDayDomain(6) }
                }
            };

            var connectingFlights = new List<ConnectingFlightsDomain>
            {
                new ConnectingFlightsDomain
                {
                    OriginFlight = originFlightDomainFirst,
                    DestinationFlight = destinationFlightDomainFirst
                },
                new ConnectingFlightsDomain
                {
                    OriginFlight = originFlightDomainSecond,
                    DestinationFlight = destinationFlightDomainSecond
                }
            };

            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(originFlightIdFirst)).ReturnsAsync(originFlightFirst);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(destinationFlightIdFirst)).ReturnsAsync(destinationFlightFirst);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(originFlightIdSecond)).ReturnsAsync(originFlightSecond);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(destinationFlightIdSecond)).ReturnsAsync(destinationFlightSecond);

            // Act
            var featuredFlight = await _flightsService.GetFeaturedFlightAsync(connectingFlights);

            // Assert
            Assert.NotNull(featuredFlight);
            Assert.Equal(2, featuredFlight.Day);
            Assert.Equal(3, featuredFlight.FlightCombination.OriginFlight.Id);
            Assert.Equal(4, featuredFlight.FlightCombination.DestinationFlight.Id);
        }

        [Fact]
        public async Task GetFeaturedFlight_DaysAreDifferentTimeDifferenceIsDifferentByOneDay_ReturnsFlightsCombination()
        {
            // Arrange            
            var originAirtportId = 5;
            var destinationAirportId = 8;
            var commonAirportIdFirst = 11;
            var commonAirportIdSecond = 12;
            var originFlightIdFirst = 1;
            var destinationFlightIdFirst = 2;
            var originFlightIdSecond = 3;
            var destinationFlightIdSecond = 4;
            
            var originFlightFirst = new Flight
            {
                FlightId = originFlightIdFirst,
                DepartureTime = "22:00",
                LandingTime = "02:00",
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(1)
                }
            };

            var destinationFlightFirst = new Flight
            {
                FlightId = destinationFlightIdFirst,
                DepartureTime = "08:00", // 6 hrs difference
                LandingTime = "12:00",
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(2)
                }
            };

            var originFlightSecond = new Flight
            {
                FlightId = originFlightIdSecond,
                DepartureTime = "02:00",
                LandingTime = "06:00",
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(2)
                }
            };

            var destinationFlightSecond = new Flight
            {
                FlightId = destinationFlightIdSecond,
                DepartureTime = "15:00", // 9 hrs difference
                LandingTime = "17:00",
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(2)
                }
            };

            var originFlightDomainFirst = new FlightUpdateDomain
            {
                Id = originFlightIdFirst,
                OriginAirportId = originAirtportId,
                DestinationAirportId = commonAirportIdFirst,
                DepartureTime = "22:00",
                LandingTime = "02:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day = GetDayDomain(1)}
                }
            };

            var destinationFlightDomainFirst = new FlightUpdateDomain
            {
                Id = destinationFlightIdFirst,
                OriginAirportId = commonAirportIdFirst,
                DestinationAirportId = destinationAirportId,
                DepartureTime = "08:00", // 6 hrs difference
                LandingTime = "12:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day = GetDayDomain(2)}
                }
            };

            var originFlightDomainSecond = new FlightUpdateDomain
            {
                Id = originFlightIdSecond,
                OriginAirportId = originAirtportId,
                DestinationAirportId = commonAirportIdSecond,
                DepartureTime = "02:00",
                LandingTime = "06:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day = GetDayDomain(2)}
                }
            };

            var destinationFlightDomainSecond = new FlightUpdateDomain
            {
                Id = destinationFlightIdSecond,
                OriginAirportId = commonAirportIdSecond,
                DestinationAirportId = destinationAirportId,
                DepartureTime = "15:00", // 9 hrs difference
                LandingTime = "17:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day = GetDayDomain(2)}
                }
            };

            var connectingFlights = new List<ConnectingFlightsDomain>
            {
                new ConnectingFlightsDomain
                {
                    OriginFlight = originFlightDomainFirst,
                    DestinationFlight = destinationFlightDomainFirst
                },
                new ConnectingFlightsDomain
                {
                    OriginFlight = originFlightDomainSecond,
                    DestinationFlight = destinationFlightDomainSecond
                }
            };

            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(originFlightIdFirst)).ReturnsAsync(originFlightFirst);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(destinationFlightIdFirst)).ReturnsAsync(destinationFlightFirst);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(originFlightIdSecond)).ReturnsAsync(originFlightSecond);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(destinationFlightIdSecond)).ReturnsAsync(destinationFlightSecond);

            // Act
            var featuredFlight = await _flightsService.GetFeaturedFlightAsync(connectingFlights);

            // Assert
            Assert.NotNull(featuredFlight);
            Assert.Equal(1, featuredFlight.Day);
            Assert.Equal(1, featuredFlight.FlightCombination.OriginFlight.Id);
            Assert.Equal(2, featuredFlight.FlightCombination.DestinationFlight.Id);
        }

        [Fact]
        public async Task GetFeaturedFlight_DaysAreSameTimeDifferenceIsDifferentOriginLandingOtherDay_ReturnsFlightsCombination()
        {
            // Arrange            
            var originAirtportId = 5;
            var destinationAirportId = 8;
            var commonAirportIdFirst = 11;
            var commonAirportIdSecond = 12;
            var originFlightIdFirst = 1;
            var destinationFlightIdFirst = 2;
            var originFlightIdSecond = 3;
            var destinationFlightIdSecond = 4;
            
            var originFlightFirst = new Flight
            {
                FlightId = originFlightIdFirst,
                DepartureTime = "22:00",
                LandingTime = "02:00",
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(2)
                }
            };

            var destinationFlightFirst = new Flight
            {
                FlightId = destinationFlightIdFirst,
                DepartureTime = "08:00", // 6 hrs difference
                LandingTime = "12:00",
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(2)
                }
            };

            var originFlightSecond = new Flight
            {
                FlightId = originFlightIdSecond,
                DepartureTime = "02:00",
                LandingTime = "06:00",
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(2)
                }
            };

            var destinationFlightSecond = new Flight
            {
                FlightId = destinationFlightIdSecond,
                DepartureTime = "15:00", // 9 hrs difference
                LandingTime = "17:00",
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(2)
                }
            };

            var originFlightDomainFirst = new FlightUpdateDomain
            {
                Id = originFlightIdFirst,
                OriginAirportId = originAirtportId,
                DestinationAirportId = commonAirportIdFirst,
                DepartureTime = "22:00",
                LandingTime = "02:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day = GetDayDomain(2) }
                }
            };

            var destinationFlightDomainFirst = new FlightUpdateDomain
            {
                Id = destinationFlightIdFirst,
                OriginAirportId = commonAirportIdFirst,
                DestinationAirportId = destinationAirportId,
                DepartureTime = "08:00", // 6 hrs difference
                LandingTime = "12:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day = GetDayDomain(2)}
                }
            };

            var originFlightDomainSecond = new FlightUpdateDomain
            {
                Id = originFlightIdSecond,
                OriginAirportId = originAirtportId,
                DestinationAirportId = commonAirportIdSecond,
                DepartureTime = "02:00",
                LandingTime = "06:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day = GetDayDomain(2) }
                }
            };

            var destinationFlightDomainSecond = new FlightUpdateDomain
            {
                Id = destinationFlightIdSecond,
                OriginAirportId = commonAirportIdSecond,
                DestinationAirportId = destinationAirportId,
                DepartureTime = "15:00", // 9 hrs difference
                LandingTime = "17:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day = GetDayDomain(2) }
                }
            };

            var connectingFlights = new List<ConnectingFlightsDomain>
            {
                new ConnectingFlightsDomain
                {
                    OriginFlight = originFlightDomainFirst,
                    DestinationFlight = destinationFlightDomainFirst
                },
                new ConnectingFlightsDomain
                {
                    OriginFlight = originFlightDomainSecond,
                    DestinationFlight = destinationFlightDomainSecond
                }
            };

            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(originFlightIdFirst)).ReturnsAsync(originFlightFirst);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(destinationFlightIdFirst)).ReturnsAsync(destinationFlightFirst);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(originFlightIdSecond)).ReturnsAsync(originFlightSecond);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(destinationFlightIdSecond)).ReturnsAsync(destinationFlightSecond);

            // Act
            var featuredFlight = await _flightsService.GetFeaturedFlightAsync(connectingFlights);

            // Assert
            Assert.NotNull(featuredFlight);
            Assert.Equal(2, featuredFlight.Day);
            Assert.Equal(3, featuredFlight.FlightCombination.OriginFlight.Id);
            Assert.Equal(4, featuredFlight.FlightCombination.DestinationFlight.Id);
        }

        [Fact]
        public async Task GetFeaturedFlight_DaysAreSameTimeDifferenceIsNegative_ReturnsFlightsCombination()
        {
            // Arrange         
            var originAirtportId = 5;
            var destinationAirportId = 8;
            var commonAirportIdFirst = 11;
            var commonAirportIdSecond = 12;
            var originFlightIdFirst = 1;
            var destinationFlightIdFirst = 2;
            var originFlightIdSecond = 3;
            var destinationFlightIdSecond = 4;

            
            var originFlightFirst = new Flight
            {
                FlightId = originFlightIdFirst,
                DepartureTime = "22:00",
                LandingTime = "02:00",
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(2)
                }
            };

            var destinationFlightFirst = new Flight
            {
                FlightId = destinationFlightIdFirst,
                DepartureTime = "01:00", // -1 hrs difference
                LandingTime = "12:00",
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(2)
                }
            };

            var originFlightSecond = new Flight
            {
                FlightId = originFlightIdSecond,
                DepartureTime = "02:00",
                LandingTime = "06:00",
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(2)
                }
            };

            var destinationFlightSecond = new Flight
            {
                FlightId = destinationFlightIdSecond,
                DepartureTime = "15:00", // 9 hrs difference
                LandingTime = "17:00",
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(2)
                }
            };

            var originFlightDomainFirst = new FlightUpdateDomain
            {
                Id = originFlightIdFirst,
                OriginAirportId = originAirtportId,
                DestinationAirportId = commonAirportIdFirst,
                DepartureTime = "22:00",
                LandingTime = "02:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day = GetDayDomain(2) }
                }
            };

            var destinationFlightDomainFirst = new FlightUpdateDomain
            {
                Id = destinationFlightIdFirst,
                OriginAirportId = commonAirportIdFirst,
                DestinationAirportId = destinationAirportId,
                DepartureTime = "01:00", // -1 hrs difference
                LandingTime = "12:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day = GetDayDomain(2) }
                }
            };

            var originFlightDomainSecond = new FlightUpdateDomain
            {
                Id = originFlightIdSecond,
                OriginAirportId = originAirtportId,
                DestinationAirportId = commonAirportIdSecond,
                DepartureTime = "02:00",
                LandingTime = "06:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day = GetDayDomain(2)}
                }
            };

            var destinationFlightDomainSecond = new FlightUpdateDomain
            {
                Id = destinationFlightIdSecond,
                OriginAirportId = commonAirportIdSecond,
                DestinationAirportId = destinationAirportId,
                DepartureTime = "15:00", // 9 hrs difference
                LandingTime = "17:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day = GetDayDomain(2) }
                }
            };

            var connectingFlights = new List<ConnectingFlightsDomain>
            {
                new ConnectingFlightsDomain
                {
                    OriginFlight = originFlightDomainFirst,
                    DestinationFlight = destinationFlightDomainFirst
                },
                new ConnectingFlightsDomain
                {
                    OriginFlight = originFlightDomainSecond,
                    DestinationFlight = destinationFlightDomainSecond
                }
            };

            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(originFlightIdFirst)).ReturnsAsync(originFlightFirst);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(destinationFlightIdFirst)).ReturnsAsync(destinationFlightFirst);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(originFlightIdSecond)).ReturnsAsync(originFlightSecond);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(destinationFlightIdSecond)).ReturnsAsync(destinationFlightSecond);

            // Act
            var featuredFlight = await _flightsService.GetFeaturedFlightAsync(connectingFlights);

            // Assert
            Assert.NotNull(featuredFlight);
            Assert.Equal(2, featuredFlight.Day);
            Assert.Equal(3, featuredFlight.FlightCombination.OriginFlight.Id);
            Assert.Equal(4, featuredFlight.FlightCombination.DestinationFlight.Id);
        }

        [Fact]
        public async Task GetFeaturedFlight_TimeDifferenceIsSameDaysDifferenceIsDifferentOptimalDayIsSmallerByThree_ReturnsFeaturedFlight()
        {
            // Arrange            
            var originAirtportId = 5;
            var destinationAirportId = 8;
            var commonAirportIdFirst = 11;
            var commonAirportIdSecond = 12;
            var originFlightIdFirst = 1;
            var destinationFlightIdFirst = 2;
            var originFlightIdSecond = 3;
            var destinationFlightIdSecond = 4;
            
            var originFlightFirst = new Flight
            {
                FlightId = originFlightIdFirst,
                DepartureTime = "7:00",
                LandingTime = "12:00",
                FlightDays = new List<FlightDay>
                {
                    new FlightDay { DayId = 3 }
                }
            };

            var destinationFlightFirst = new Flight
            {
                FlightId = destinationFlightIdFirst,
                DepartureTime = "15:00", // 3 hrs difference
                LandingTime = "18:00",
                FlightDays = new List<FlightDay>
                {
                    new FlightDay { DayId = 1 }, 
                    new FlightDay { DayId = 7 }
                }
            };

            var originFlightSecond = new Flight
            {
                FlightId = originFlightIdSecond,
                DepartureTime = "6:00",
                LandingTime = "11:00",
                FlightDays = new List<FlightDay>
                {
                    new FlightDay { DayId = 2 }
                }
            };

            var destinationFlightSecond = new Flight
            {
                FlightId = destinationFlightIdSecond,
                DepartureTime = "14:00", // 3 hrs difference
                LandingTime = "17:00",
                FlightDays = new List<FlightDay>
                {
                    new FlightDay { DayId = 7 }
                }
            };

            var originFlightDomainFirst = new FlightUpdateDomain
            {
                Id = originFlightIdFirst,
                OriginAirportId = originAirtportId,
                DestinationAirportId = commonAirportIdFirst,
                DepartureTime = "7:00",
                LandingTime = "12:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day = GetDayDomain(3)},
                }
            };

            var destinationFlightDomainFirst = new FlightUpdateDomain
            {
                Id = destinationFlightIdFirst,
                OriginAirportId = commonAirportIdFirst,
                DestinationAirportId = destinationAirportId,
                DepartureTime = "15:00", // 3 hrs difference
                LandingTime = "18:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day =GetDayDomain(1) },
                    new FlightDayDomain {Day =GetDayDomain(7) }
                }
            };

            var originFlightDomainSecond = new FlightUpdateDomain
            {
                Id = originFlightIdSecond,
                OriginAirportId = originAirtportId,
                DestinationAirportId = commonAirportIdSecond,
                DepartureTime = "6:00",
                LandingTime = "11:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain {  Day = GetDayDomain(2) }
                }
            };

            var destinationFlightDomainSecond = new FlightUpdateDomain
            {
                Id = destinationFlightIdSecond,
                OriginAirportId = commonAirportIdSecond,
                DestinationAirportId = destinationAirportId,
                DepartureTime = "14:00", // 3 hrs difference
                LandingTime = "17:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain {  Day =GetDayDomain(7)}
                }
            };

            var connectingFlights = new List<ConnectingFlightsDomain>
            {
                new ConnectingFlightsDomain
                {
                    OriginFlight = originFlightDomainFirst,
                    DestinationFlight = destinationFlightDomainFirst
                },
                new ConnectingFlightsDomain
                {
                    OriginFlight = originFlightDomainSecond,
                    DestinationFlight = destinationFlightDomainSecond
                }
            };

            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(originFlightIdFirst)).ReturnsAsync(originFlightFirst);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(destinationFlightIdFirst)).ReturnsAsync(destinationFlightFirst);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(originFlightIdSecond)).ReturnsAsync(originFlightSecond);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(destinationFlightIdSecond)).ReturnsAsync(destinationFlightSecond);

            // Act
            var featuredFlight = await _flightsService.GetFeaturedFlightAsync(connectingFlights);

            // Assert
            Assert.NotNull(featuredFlight);
            Assert.Equal(3, featuredFlight.Day);
            Assert.Equal(1, featuredFlight.FlightCombination.OriginFlight.Id);
            Assert.Equal(2, featuredFlight.FlightCombination.DestinationFlight.Id);
        }

        [Fact]
        public async Task GetFeaturedFlight_TimeDifferenceIsSameButForSecondDayDifferenceIsSmallerByTwo_ReturnsFeaturedFlight()
        {
            // Arrange            
            var originAirtportId = 5;
            var destinationAirportId = 8;
            var commonAirportIdFirst = 11;
            var commonAirportIdSecond = 12;
            var originFlightIdFirst = 1;
            var destinationFlightIdFirst = 2;
            var originFlightIdSecond = 3;
            var destinationFlightIdSecond = 4;
            
            var originFlightFirst = new Flight
            {
                FlightId = originFlightIdFirst,

                DepartureTime = "7:00",
                LandingTime = "12:00",
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(7),
                    GetFlightDay(6)
                }
            };

            var destinationFlightFirst = new Flight
            {
                FlightId = destinationFlightIdFirst,
                DepartureTime = "15:00", // 3 hrs difference
                LandingTime = "18:00",
                FlightDays = new List<FlightDay>
                {
                     GetFlightDay(4),
                     GetFlightDay(2)
                }
            };

            var originFlightSecond = new Flight
            {
                FlightId = originFlightIdSecond,
                DepartureTime = "6:00",
                LandingTime = "11:00",
                FlightDays = new List<FlightDay>
                {
                   // new FlightDay { Day = new Day { DayId = 6, Name = "uto" }, DayId = 6 },
                    GetFlightDay(2)
                }
            };

            var destinationFlightSecond = new Flight
            {
                FlightId = destinationFlightIdSecond,
                DepartureTime = "14:00", // 3 hrs difference
                LandingTime = "17:00",
                FlightDays = new List<FlightDay>
                {
                     GetFlightDay(6), //greater day
                     GetFlightDay(5)
                }
            };

            var originFlightDomainFirst = new FlightUpdateDomain
            {
                Id = originFlightIdFirst,
                OriginAirportId = originAirtportId,
                DestinationAirportId = commonAirportIdFirst,
                DepartureTime = "7:00",
                LandingTime = "12:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day = GetDayDomain(7)},
                    new FlightDayDomain { Day = GetDayDomain(6) }
                }
            };

            var destinationFlightDomainFirst = new FlightUpdateDomain
            {
                Id = destinationFlightIdFirst,
                OriginAirportId = commonAirportIdFirst,
                DestinationAirportId = destinationAirportId,
                DepartureTime = "15:00", // 3 hrs difference
                LandingTime = "18:00",
                FlightDays = new List<FlightDayDomain>
                {
                     new FlightDayDomain { Day = GetDayDomain(4)},
                     new FlightDayDomain { Day = GetDayDomain(2) }
                }
            };

            var originFlightDomainSecond = new FlightUpdateDomain
            {
                Id = originFlightIdSecond,
                OriginAirportId = originAirtportId,
                DestinationAirportId = commonAirportIdSecond,
                DepartureTime = "6:00",
                LandingTime = "11:00",
                FlightDays = new List<FlightDayDomain>
                {
                    // new FlightDay { Day = new Day { DayId = 6, Name = "uto" }, DayId = 6 },
                    new FlightDayDomain { Day = GetDayDomain(2) }
                }
            };

            var destinationFlightDomainSecond = new FlightUpdateDomain
            {
                Id = destinationFlightIdSecond,
                OriginAirportId = commonAirportIdSecond,
                DestinationAirportId = destinationAirportId,
                DepartureTime = "14:00", // 3 hrs difference
                LandingTime = "17:00",
                FlightDays = new List<FlightDayDomain>
                {
                     new FlightDayDomain { Day = GetDayDomain(6)}, //greater day
                     new FlightDayDomain { Day = GetDayDomain(5) }
                }
            };

            var connectingFlights = new List<ConnectingFlightsDomain>
            {
                new ConnectingFlightsDomain
                {
                    OriginFlight = originFlightDomainFirst,
                    DestinationFlight = destinationFlightDomainFirst
                },
                new ConnectingFlightsDomain
                {
                    OriginFlight = originFlightDomainSecond,
                    DestinationFlight = destinationFlightDomainSecond
                }
            };

            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(originFlightIdFirst)).ReturnsAsync(originFlightFirst);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(destinationFlightIdFirst)).ReturnsAsync(destinationFlightFirst);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(originFlightIdSecond)).ReturnsAsync(originFlightSecond);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(destinationFlightIdSecond)).ReturnsAsync(destinationFlightSecond);

            // Act
            var featuredFlight = await _flightsService.GetFeaturedFlightAsync(connectingFlights);

            // Assert
            Assert.NotNull(featuredFlight);
            Assert.Equal(7, featuredFlight.Day);
            Assert.Equal(1, featuredFlight.FlightCombination.OriginFlight.Id);
            Assert.Equal(2, featuredFlight.FlightCombination.DestinationFlight.Id);
        }

        [Fact]           // method name _ what we are testing (data state) _ what is expected result
        public async Task GetFeaturedFlight_TimeDifferenceIsSmallerButdDaysDifferenceIsGreater_ReturnsFeaturedFlight()
        {
            // Arrange            
            var originAirtportId = 5;
            var destinationAirportId = 8;
            var commonAirportIdFirst = 11;
            var commonAirportIdSecond = 12;
            var originFlightIdFirst = 1;
            var destinationFlightIdFirst = 2;
            var originFlightIdSecond = 3;
            var destinationFlightIdSecond = 4;
            
            var originFlightFirst = new Flight
            {
                FlightId = originFlightIdFirst,
                
                DepartureTime = "7:00",
                LandingTime = "12:00",
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(1),
                    GetFlightDay(6)
                }
            };

            var destinationFlightFirst = new Flight
            {
                FlightId = destinationFlightIdFirst,
                DepartureTime = "15:00", // 3 hrs difference
                LandingTime = "18:00",
                FlightDays = new List<FlightDay>
                {
                     GetFlightDay(1), //different day
                     GetFlightDay(7)
                }
            };

            var originFlightSecond = new Flight
            {
                FlightId = originFlightIdSecond,
                DepartureTime = "6:00",
                LandingTime = "11:00",
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(1),
                    GetFlightDay(6)
                }
            };

            var destinationFlightSecond = new Flight
            {
                FlightId = destinationFlightIdSecond,
                DepartureTime = "12:30", // 1,5 hrs difference
                LandingTime = "17:00",
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(3), // different day
                    GetFlightDay(7)
                }
            };

            var originFlightDomainFirst = new FlightUpdateDomain
            {
                Id = originFlightIdFirst,
                OriginAirportId = originAirtportId,
                DestinationAirportId = commonAirportIdFirst,
                DepartureTime = "7:00",
                LandingTime = "12:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day = GetDayDomain(1) },
                    new FlightDayDomain { Day = GetDayDomain(6) }
                }
            };

            var destinationFlightDomainFirst = new FlightUpdateDomain
            {
                Id = destinationFlightIdFirst,
                OriginAirportId = commonAirportIdFirst,
                DestinationAirportId = destinationAirportId,
                DepartureTime = "15:00", // 3 hrs difference
                LandingTime = "18:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day = GetDayDomain(1) }, //same day
                    new FlightDayDomain { Day = GetDayDomain(7) }
                }
            };

            var originFlightDomainSecond = new FlightUpdateDomain
            {
                Id = originFlightIdSecond,
                OriginAirportId = originAirtportId,
                DestinationAirportId = commonAirportIdSecond,
                DepartureTime = "6:00",
                LandingTime = "11:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day = GetDayDomain(2) }, 
                    new FlightDayDomain { Day = GetDayDomain(6) }
                }
            };

            var destinationFlightDomainSecond = new FlightUpdateDomain
            {
                Id = destinationFlightIdSecond,
                OriginAirportId = commonAirportIdSecond,
                DestinationAirportId = destinationAirportId,
                DepartureTime = "12:30", // 1,5 hrs difference
                LandingTime = "17:00",
                FlightDays = new List<FlightDayDomain>
                {
                     new FlightDayDomain { Day = GetDayDomain(3) }, //different day
                     new FlightDayDomain { Day = GetDayDomain(7) }
                }
            };

            var connectingFlights = new List<ConnectingFlightsDomain>
            {
                new ConnectingFlightsDomain
                {
                    OriginFlight = originFlightDomainFirst,
                    DestinationFlight = destinationFlightDomainFirst
                },
                new ConnectingFlightsDomain
                {
                    OriginFlight = originFlightDomainSecond,
                    DestinationFlight = destinationFlightDomainSecond
                }
            };

            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(originFlightIdFirst)).ReturnsAsync(originFlightFirst);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(destinationFlightIdFirst)).ReturnsAsync(destinationFlightFirst);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(originFlightIdSecond)).ReturnsAsync(originFlightSecond);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(destinationFlightIdSecond)).ReturnsAsync(destinationFlightSecond);

            // Act
            var featuredFlight = await _flightsService.GetFeaturedFlightAsync(connectingFlights);

            // Assert
            Assert.NotNull(featuredFlight);
            Assert.Equal(1, featuredFlight.Day);
            Assert.Equal(1, featuredFlight.FlightCombination.OriginFlight.Id);
            Assert.Equal(2, featuredFlight.FlightCombination.DestinationFlight.Id);       
        }

        [Fact]
        public async Task GetFeaturedFlight_DaysDifferenceIsSameButTimeDifferenceIsDifferent_ReturnsFlightsCombination()
        {
            // Arrange         
            var originAirtportId = 5;
            var destinationAirportId = 8;
            var commonAirportIdFirst = 11;
            var commonAirportIdSecond = 12;
            var originFlightIdFirst = 1;
            var destinationFlightIdFirst = 2;
            var originFlightIdSecond = 3;
            var destinationFlightIdSecond = 4;
                       
            var originFlightFirst = new Flight
            {
                FlightId = originFlightIdFirst,
                DepartureTime = "7:00",
                LandingTime = "12:00",
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(2),
                    GetFlightDay(1)
                }
            };

            var destinationFlightFirst = new Flight
            {
                FlightId = destinationFlightIdFirst,
                DepartureTime = "13:00", // 1 hrs difference
                LandingTime = "18:00",
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(4),
                    GetFlightDay(6)
                }
            };

            var originFlightSecond = new Flight
            {
                FlightId = originFlightIdSecond,
                DepartureTime = "6:00",
                LandingTime = "11:00",
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(3),
                    GetFlightDay(2)
                }
            };

            var destinationFlightSecond = new Flight
            {
                FlightId = destinationFlightIdSecond,
                DepartureTime = "14:00", // 3 hrs difference
                LandingTime = "17:00",
                FlightDays = new List<FlightDay>
                {
                    GetFlightDay(5),
                    GetFlightDay(7)
                }
            };

            var originFlightDomainFirst = new FlightUpdateDomain
            {
                Id = originFlightIdFirst,
                OriginAirportId = originAirtportId,
                DestinationAirportId = commonAirportIdFirst,
                DepartureTime = "7:00",
                LandingTime = "12:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day = GetDayDomain(2) },
                    new FlightDayDomain { Day = GetDayDomain(1)}
                },
            };

            var destinationFlightDomainFirst = new FlightUpdateDomain
            {
                Id = destinationFlightIdFirst,
                OriginAirportId = commonAirportIdFirst,
                DestinationAirportId = destinationAirportId,
                DepartureTime = "13:00", // 1 hrs difference
                LandingTime = "18:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day = GetDayDomain(4) },
                    new FlightDayDomain { Day = GetDayDomain(6) }
                }
            };

            var originFlightDomainSecond = new FlightUpdateDomain
            {
                Id = originFlightIdSecond,
                OriginAirportId = originAirtportId,
                DestinationAirportId = commonAirportIdSecond,
                DepartureTime = "6:00",
                LandingTime = "11:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day = GetDayDomain(3) },
                    new FlightDayDomain { Day = GetDayDomain(2) }
                }
            };

            var destinationFlightDomainSecond = new FlightUpdateDomain
            {
                Id = destinationFlightIdSecond,
                OriginAirportId = commonAirportIdSecond,
                DestinationAirportId = destinationAirportId,
                DepartureTime = "14:00", // 3 hrs difference
                LandingTime = "17:00",
                FlightDays = new List<FlightDayDomain>
                {
                    new FlightDayDomain { Day = GetDayDomain(5) },
                    new FlightDayDomain { Day = GetDayDomain(7) }
                }
            };

            var connectingFlights = new List<ConnectingFlightsDomain>
            {
                new ConnectingFlightsDomain
                {
                    OriginFlight = originFlightDomainFirst,
                    DestinationFlight = destinationFlightDomainFirst
                },
                new ConnectingFlightsDomain
                {
                    OriginFlight = originFlightDomainSecond,
                    DestinationFlight = destinationFlightDomainSecond
                }
            };

            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(originFlightIdFirst)).ReturnsAsync(originFlightFirst);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(destinationFlightIdFirst)).ReturnsAsync(destinationFlightFirst);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(originFlightIdSecond)).ReturnsAsync(originFlightSecond);
            _flightsDataAccessMock.Setup(x => x.FindByIdAsync(destinationFlightIdSecond)).ReturnsAsync(destinationFlightSecond);

            // Act         
            var featuredFlight = await _flightsService.GetFeaturedFlightAsync(connectingFlights);

            // Assert
            Assert.NotNull(featuredFlight);
            Assert.Equal(2, featuredFlight.Day);
            Assert.Equal(1, featuredFlight.FlightCombination.OriginFlight.Id);
            Assert.Equal(2, featuredFlight.FlightCombination.DestinationFlight.Id);
        }
    }
}
