﻿using System;

namespace BinaryTree
{
    class Program
    {
        static void Main(string[] args)
        {
            Tree tree = new Tree();
            Node root = new Node();

            tree.Add(4);
            tree.Add(2);
            tree.Add(5);
            tree.Add(1);
            tree.Add(3);
            tree.DisplayTree();

            tree.SearchNode(tree.top, 2);
            if (Tree.flag)
            {
                Console.WriteLine("Element exists.");
            }
            else
                Console.WriteLine("Element does not exist.");

            tree.SearchNode(tree.top, 7);
            if (Tree.flag)
            {
                Console.WriteLine("Element exists.");
            }
            else
                Console.WriteLine("Element does not exist.");

            tree.SearchNode(tree.top, 1);
            if (Tree.flag)
            {
                Console.WriteLine("Element exists.");
            }
            else
                Console.WriteLine("Element does not exist.");


            Node n = tree.SearchNode1(tree.top, 2);
            Console.WriteLine(n.value);
            Node n1 = tree.SearchNode1(tree.top, 10);
            Console.WriteLine(n1.value);

            Console.ReadKey();
        }
    }
}
