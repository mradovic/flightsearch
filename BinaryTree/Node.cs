﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryTree
{
    public class Node
    {

        public int value;
        public Node left;
        public Node right;

        public Node()
        {

        }
        public Node(int initial)
        {
            value = initial;
            left = null;
            right = null;
        }

    }
}
