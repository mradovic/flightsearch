﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryTree
{
    public class Tree
    {
        public Node top;

        public static bool flag = false;

        public Tree()
        {
            top = null;
        }

        public Tree(int initial)
        {
            top = new Node(initial);
        }

        public void Add(int value)
        {
            if (top == null) //tree is empty
            {
                top = new Node(value);

                return;
            }

            AddRec(top, new Node(value));
        }

        private void AddRec(Node root, Node newNode)
        {
            if(root == null)
            {
                root = newNode;
            }

            if(newNode.value < root.value)
            {
                if (root.left == null)
                    root.left = newNode;
                else
                    AddRec(root.left, newNode);
            }

            else
            {
                if (root.right == null)
                    root.right = newNode;
                else
                    AddRec(root.right, newNode);
            }
        }

        public void FindByValue(int value)
        {

            if (top == null)
                return;

        }

        private void DisplayTree(Node root)
        {
            if (root == null) return;

            DisplayTree(root.left);
            Console.WriteLine(root.value + " ");
            DisplayTree(root.right);
        }

        public void DisplayTree()
        {
            DisplayTree(top);
        }

        public void SearchNode(Node temp, int value)
        {
            flag = false;

            if (top == null)
            {
                Console.WriteLine("Tree is empty");
                return;
            } 
            else
            {
                if (temp.value.Equals(value))
                {
                    flag = true;
                    return;
                }

                if(flag == false && temp.left != null)
                {
                    SearchNode(temp.left, value);
                }

                if (flag == false && temp.right != null)
                {
                    SearchNode(temp.right, value);
                }
            }
        }


        public Node SearchNode1(Node temp, int value)
        {
            if (temp == null)
                return new Node(-1);
            if( temp.value == value)
            {
                return temp;
            }
            // val is greater than root's key 
            if (temp.value > value)
                return SearchNode1(temp.left, value);

            // val is less than root's key 
            return SearchNode1(temp.right, value);
        }

    }
}
