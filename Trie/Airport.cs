﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Trie
{
    public class Airport
    {
        public int Id { get; set; }

        public string Code { get; set; }
    }
}
