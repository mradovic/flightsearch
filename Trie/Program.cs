﻿using sun.text.normalizer;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Trie
{
    public class Program
    {
        private const string baseURL = "http://localhost:5000/";
        private static string urlParameters = "api/airports/all";
        private static Dictionary<string, int> airports;

        public static HttpClient GetHttpClient()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(baseURL);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }

        public static Dictionary<string, int> GetAirports()
        {
            Dictionary<string, int> airportsDictionary = new Dictionary<string, int>();
            try
            {
                using (var client = GetHttpClient())
                {
                    // Get countries
                    HttpResponseMessage response = client.GetAsync(urlParameters).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        // Parse the response body.
                        var airports = response.Content.ReadAsAsync<List<Airport>>().Result;
                        foreach (var item in airports)
                        {
                            airportsDictionary.Add(item.Code, item.Id);
                        }
                    }
                    else
                    {
                        Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                    }
                    return airportsDictionary;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return airportsDictionary;
            }
        }


        static void Main(string[] args)
        {

            //Trie<string> trie = new Trie<string>();

            ////Add some key-value pairs to the trie
            //trie.Put("James", "112");
            //trie.Put("Jake", "222");
            //trie.Put("Fred", "326");

            Console.WriteLine("Hello World!");
        }
    }
}
