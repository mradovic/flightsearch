﻿using System;
using System.Collections.Generic;

namespace Trie
{
    public class Node
    {
        public int? Id { get; set; }
        public string Value { get; set; }
        public List<Node> Children { get; set; }
        //public Node Parent { get; set; }
        //public int Depth { get; set; }

        public Node(string value, Node parent)
        {
            Value = value;
            Children = new List<Node>();
            //Depth = depth;
            //Parent = parent;
        }

        
    }
}
