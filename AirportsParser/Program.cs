﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AirportsParser
{
    public class Program
    {
        public static void WriteCountriesAndAirportsToSql(string airportsInput, string countriesOutput, string airportsOutput)
        {
            StreamWriter countriesWriter;
            StreamWriter airportsWriter;

            using (countriesWriter = new StreamWriter(countriesOutput))
            using (airportsWriter = new StreamWriter(airportsOutput))
            {
                string[] lines = File.ReadAllLines(airportsInput);
                var countries = new Dictionary<string, int?>();
                int counter = 0;
                string countrySqlRow;
                string airportSqlRow;
                int? countryId;

                foreach (string line in lines)
                {
                    string trimmedLine = line.Replace("\"", "");
                    string[] array = trimmedLine.Split(",");
                    string country = null;
                    string code = null;
                    string city = null;
                    string description = null;

                    //length 4
                    if (array.Length == 4)
                    {
                        city = array[0].Replace("'", "''");
                        description = array[1].Trim().Replace("'", "''");
                        country = array[2].Trim().Replace("'", "''"); ;
                        code = array[3];
                    }
                    //length 3
                    else if (array.Length == 3)
                    {
                        city = array[0].Replace("'", "''");
                        country = array[1].Trim().Replace("'", "''"); ;
                        code = array[2];

                        if (array[1].Contains('-'))
                        {
                            string[] countryDescription = array[1].Split('-');
                            country = countryDescription[0].Trim().Replace("'", "''");
                            description = countryDescription[1].Trim().Replace("'", "''");
                        }
                    }
                    //length 2
                    else
                    {
                        country = array[0];
                        if (array[0].Contains('-'))
                        {
                            string[] countryDescription = array[0].Split('-');
                            country = countryDescription[0].Trim().Replace("'", "''");
                            description = countryDescription[1].Trim().Replace("'", "''");
                        }
                        code = array[1];
                    }

                    //country does not exist
                    if (countries.GetValueOrDefault(country) == null)
                    {
                        counter++;
                        countryId = counter;
                        countrySqlRow = $"INSERT INTO [FlightSearch].[Countries] ([CountryId], [Name]) VALUES ({countryId},\'{country}\')";
                        countriesWriter.WriteLine(countrySqlRow);
                        countries.Add(country, counter);
                    }
                    //country exists
                    else
                    {
                        countryId = countries.GetValueOrDefault(country);
                    }

                    airportSqlRow = $"INSERT INTO [FlightSearch].[Airports] ([Code], [City], [Description], [CountryId]) VALUES (\'{code}\',\'{city}\',\'{description}\',{countryId})";
                    airportsWriter.WriteLine(airportSqlRow);
                }
            }
        }

        static void Main(string[] args)
        {
            string airportCodes = @"../../../..//Tools/Files/airport-codes.csv";
            string countriesSql = @"../../../..//Tools/Files/countriesSql.txt";
            string airportsSql = @"../../../..//Tools/Files/airportsSql.txt";
            WriteCountriesAndAirportsToSql(airportCodes, countriesSql, airportsSql);
        }
    }
}
