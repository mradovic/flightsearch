﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightsParser
{
    public class Airport
    {
        public int Id { get; set; }

        public string Code { get; set; }
    }
}
