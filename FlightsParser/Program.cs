﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;

namespace FlightsParser
{
    class Program
    {
        private const string baseURL = "http://localhost:5000/";
        private static string urlParameters = "api/airports/all";
        private static List<Flight> flights;
        private static Dictionary<string, int> airports;
        private static string daysOutput = @"../../../..//Tools/Files/daysSql.txt";
        private static string flightsInput = @"../../../..//Tools/Files/flights.txt";
        private static string flightsOutput = @"../../../..//Tools/Files/flightsSQL.txt";
        private static string flightDaysOutput = @"../../../..//Tools/Files/flightDaysSQL.txt";

        public static void WriteFlights()
        {
            airports = GetAirports();
            StreamReader flightsReader;
            StreamWriter flightsWriter;
            StreamWriter flightDaysWriter = null;

            try
            {
                flightsReader = new StreamReader(flightsInput);
               
                using (flightsWriter = new StreamWriter(flightsOutput))
                using  (flightDaysWriter = new StreamWriter(flightDaysOutput))
                {
                    var flightLine = flightsReader.ReadLine();
                    flights = new List<Flight>();

                    while (flightLine != null)
                    {
                        var array = flightLine.Split("|");
                        string code = array[0];
                        string origin = array[1];
                        string destination = array[2];
                        string departureTime = array[3];
                        string landingTime = array[4];
                        string agency = array[5].Replace("'", "''");
                        string days = array[6];
                        if (string.IsNullOrWhiteSpace(days))
                        {
                            flightLine = flightsReader.ReadLine();
                            continue;
                        }
                        var rowColumns = array[7].Split('/');
                        int rows = int.Parse(rowColumns[0]);
                        int columns = int.Parse(rowColumns[1]);
                        int price = int.Parse(array[8]);

                        Flight flight = new Flight(code, origin, destination, departureTime, landingTime, agency, days, rows, columns, price);
                        flights.Add(flight);
                        flightLine = flightsReader.ReadLine();
                    }
                   
                    var daysList = GetDaysDictonary();
                    int flightCounter = 0;
                    int daysCounter = 0;
                    foreach (Flight flight in flights)
                    {
                        string sqlRowFlight;
                        flightCounter++;
                        int? originId;
                        int? destinationId;
                        string originIdString;
                        string destinationIdString;
                        originId = airports.GetValueOrDefault(flight.origin);
                        destinationId = airports.GetValueOrDefault(flight.destination);
                        originIdString = originId.ToString();
                        destinationIdString = destinationId.ToString();

                        if(originId == 0 || destinationId == 0)
                        {
                            continue;
                        }
                        sqlRowFlight = $"INSERT INTO [FlightSearch].[Flights] ([FlightId], [Code], [OriginAirportId], [DestinationAirportId], [DepartureTime], [LandingTime], [Agency], [Rows], [Columns], [Price]) VALUES (" +
                          $"{flightCounter},\'{flight.code}\', {originIdString}, {destinationIdString}, \'{flight.departureTime}\', \'{flight.landingTime}\', \'{flight.agency}\', {flight.rows}, {flight.columns}, {flight.price})\n";
                        if (flightCounter % 4000 == 0)
                        {
                            sqlRowFlight += "GO\n";
                        }

                        flightsWriter.Write(sqlRowFlight);

                        if (!string.IsNullOrEmpty(flight.days))
                        {
                            string sqlRowFlightDays;
                            string[] flightDays = flight.days.Split(',');
                               
                                if (flightDays.Length > 0)
                                {
                                    foreach (var item in flightDays)
                                    {
                                        daysCounter++;
                                        var dayId = daysList.GetValueOrDefault(item);
                                        int flightId = flightCounter;
                                        sqlRowFlightDays = $"INSERT INTO [FlightSearch].[FlightDays] ([FlightId], [DayId]) VALUES ({flightId},{dayId})\n";
                                        if (daysCounter % 10000 == 0)
                                        {
                                            sqlRowFlightDays += "GO\n";
                                        }
                                        flightDaysWriter.Write(sqlRowFlightDays);
                                    }
                                }   
                        }
                    }
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static HttpClient GetHttpClient()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(baseURL);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }

        public static Dictionary<string, int> GetAirports()
        {
            Dictionary<string, int> airportsDictionary = new Dictionary<string, int>();
            try
            {
                using (var client = GetHttpClient())
                {
                    // Get countries
                    HttpResponseMessage response = client.GetAsync(urlParameters).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        // Parse the response body.
                        var airports = response.Content.ReadAsAsync<List<Airport>>().Result;
                        foreach (var item in airports)
                        {
                            airportsDictionary.Add(item.Code, item.Id);
                        }
                    }
                    else
                    {
                        Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                    }
                    return airportsDictionary;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return airportsDictionary;
            }
        }

        public static Dictionary<string, int> GetDaysDictonary()
        {
            var days = new Dictionary<string, int>(){
                {"pon", 1},
                {"uto", 2},
                {"sre", 3},
                {"cet", 4},
                {"pet", 5},
                {"sub", 6},
                {"ned", 7},
            };
            return days;
        }

        public static void WriteDaysToSql()
        {
            StreamWriter daysWriter = null;
            var days = GetDaysDictonary();
            try
            {
                daysWriter = new StreamWriter(daysOutput);
                foreach (var day in days)
                {
                    string sqlRowAirport = $"INSERT INTO [FlightSearch].[Days] ([DayId], [Name]) VALUES (" + day.Value + ", \'" + day.Key + "\')\n";
                    daysWriter.Write(sqlRowAirport);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (daysWriter != null)
                {
                    daysWriter.Close();
                }
            }
        }

        public static void Main(string[] args)
        {
            WriteDaysToSql();
            WriteFlights();

            Console.ReadKey();
        }
    }
}


