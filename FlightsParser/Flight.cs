﻿
struct Flight
{
    public string code;
    public string origin;
    public string destination;
    public string departureTime;
    public string landingTime;
    public string agency;
    public string days;
    public int rows;
    public int columns;
    public int price;

    public Flight(string code, string origin, string destination, string departureTime, string
         landingTime, string agency, string days, int rows, int columns, int price)
    {
        this.code = code;
        this.origin = origin;
        this.destination = destination;
        this.departureTime = departureTime;
        this.landingTime = landingTime;
        this.days = days;
        this.rows = rows;
        this.columns = columns;
        this.price = price;
        this.agency = agency;
    }
}

