﻿using System;

namespace FlightSearch.Common.Extensions
{
    public class NotFoundException : Exception
    {
        public NotFoundException()
           : base()
        {
        }
 
        public NotFoundException(string message)
            : base(message)
        {
        }
    }
}
