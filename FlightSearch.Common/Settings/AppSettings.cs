﻿namespace FlightSearch.Common.Settings
{
    public class AppSettings
    {
        public string NoFlight { get; set; }

        public string NoOrigin { get; set; }

        public string NoDestination { get; set; }

        public string FoundCode { get; set; }

        public string FoundUser { get; set; }
    }
}
