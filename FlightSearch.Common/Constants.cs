﻿namespace FlightSearch.Common
{
    public static class Constants
    {
        public const string DatabaseSchemaName = "FlightSearch";
        public const int NumOfWeekDays = 7;
        public const int MinNumOfDays = 1;
        public const string Time = "HH:mm";
    }
}
