﻿using FlightSearch.Common.Settings;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace FlightSearch.Api.Application
{
    [Route("api/[controller]")]
    [ApiController]
    public class AbstractController : ControllerBase
    {
        protected AppSettings Settings { get; private set; }

        public AbstractController(IOptions<AppSettings> appSettings)
        {
            Settings = appSettings.Value;
        }
    }
}