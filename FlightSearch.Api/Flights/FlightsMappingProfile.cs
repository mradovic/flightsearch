﻿using AutoMapper;
using FlightSearch.Service.Flights;

namespace FlightSearch.Api.Flights
{
    public class FlightsMappingProfile:Profile
    {
        public FlightsMappingProfile()
        {
            CreateMap<FlightsUpdateDto, FlightDomain>();
            CreateMap<FlightCreateDto, FlightDomain>();
        }
    }
}
