﻿using FlightSearch.Common;
using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace FlightSearch.Api.Flights
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = true)]
    sealed public class ValidateTime: ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            string time = Convert.ToString(value);

            if (!string.IsNullOrWhiteSpace(time) && !DateTime.TryParseExact(time, Constants.Time,
                   CultureInfo.InvariantCulture, DateTimeStyles.None, out _))
            {
                return false;
            }
            return true;
        }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(CultureInfo.CurrentCulture,
            ErrorMessageString, name) + ". It must be in HH:mm format.";
        }
    }
}
