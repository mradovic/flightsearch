﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FlightSearch.Api.Flights
{
    public class FlightsUpdateDto
    {
        public int Id { get; set; }

        [RegularExpression(@"^[A]+[A]+[0-9]{4,8}$",
            ErrorMessage = "Code is not in the required form. (AAddddd) - Two capital A and 4 to 6 digits")]
        public string Code { get; set; }

        public int? OriginAirportId { get; set; }

        public int? DestinationAirportId { get; set; }

        [ValidateTime]
        public string DepartureTime { get; set; }

        [ValidateTime]
        public string LandingTime { get; set; }

        [StringLength(130, MinimumLength = 3,
        ErrorMessage = "Length of {0} must be between {2} and {1}.")]
        public string Agency { get; set; }

        [Range(2, 30,
        ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int? Rows { get; set; }

        [Range(2, 25,
        ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int? Columns { get; set; }

        [Range(10, 3000,
        ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int? Price { get; set; }

        [ValidateDays]
        public List<int> Days { get; set; }
    }
}
