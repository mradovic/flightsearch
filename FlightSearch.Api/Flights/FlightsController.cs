﻿using AutoMapper;
using FlightSearch.Api.Application;
using FlightSearch.Common.Settings;
using FlightSearch.Service.Flights;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace FlightSearch.Api.Flights
{
    public class FlightsController : AbstractController
    {
        private IFlightsService _fligthsService;
        private IMapper _mapper;

        public FlightsController(
            IOptions<AppSettings> appSettings,
            IFlightsService flightsService,
            IMapper mapper)
            : base(appSettings)
        {
            _fligthsService = flightsService;
            _mapper = mapper;
        }

        [HttpGet("origin/{originId}/destination/{destinationId}")]
        public async Task<IActionResult> Get(int originId, int destinationId)
        {
            var flights = await _fligthsService.GetByOriginAndDestinationAsync(originId, destinationId);
            
            return Ok(flights);
        }

        [HttpDelete("delete/{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            FlightDomain flight = await _fligthsService.GetByIdAsync(id);
            if (flight == null)
            {
                return NotFound();
            }
            await _fligthsService.RemoveAsync(id);
            return NoContent();
        }

        [HttpPut("update/{id}")]
        public async Task<ActionResult> Put(int id, [FromBody]FlightsUpdateDto flightDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            if (id != flightDto.Id || flightDto == null)
            {
                return BadRequest();
            }

            try
            {
                FlightDomain dto = _mapper.Map<FlightDomain>(flightDto);
                FlightUpdateDomain flight = await _fligthsService.EditAsync(id, dto);
                return Ok(flight);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("create")]
        public async Task<ActionResult> Post([FromBody]FlightCreateDto flightDomain)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                FlightDomain domain = _mapper.Map<FlightDomain>(flightDomain);
                FlightUpdateDomain flight = await _fligthsService.CreateAsync(domain);
                return Ok(flight);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}