﻿using AutoMapper;
using FlightSearch.Api.Application;
using FlightSearch.Common.Settings;
using FlightSearch.Service.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace FlightSearch.Api.Users
{

    public class UsersController : AbstractController
    {
        private IMapper _mapper;
        private IUsersService _usersService;

        public UsersController(
            IUsersService usersService,
            IOptions<AppSettings> appSettings,
            IMapper mapper)
            : base(appSettings)
        {
            _mapper = mapper;
            _usersService = usersService;
        }

        [HttpGet("all")]
        public async Task<IActionResult> Get()
        {
            var users = await _usersService.GetAllAsync();
            return Ok(users);
        }



        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<IActionResult> RegisterAsync([FromBody] RegisterRequestDto registerRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var request = _mapper.Map<ReqisterRequestDomain>(registerRequest);

            try
            {
                var user = await _usersService.RegisterAsync(request);
                var result = _mapper.Map<RegisterResponseDto>(user);

                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}