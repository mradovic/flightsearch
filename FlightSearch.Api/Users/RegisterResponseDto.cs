﻿namespace FlightSearch.Api.Users
{
    public class RegisterResponseDto
    {
        public int UserId { get; set; }

        public string Username { get; set; }
    }
}
