﻿using System.ComponentModel.DataAnnotations;

namespace FlightSearch.Api.Users
{
    public class RegisterRequestDto
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
