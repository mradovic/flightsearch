﻿using AutoMapper;
using FlightSearch.DataAccess.Models;
using FlightSearch.Service.Users;

namespace FlightSearch.Api.Users
{
    public class UsersMappingProfile: Profile
    {
        public UsersMappingProfile()
        {
            CreateMap<RegisterRequestDto, ReqisterRequestDomain>();
            CreateMap<RegisterResponseDomain, RegisterResponseDto>();
        }
    }
}
