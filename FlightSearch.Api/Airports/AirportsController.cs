﻿using FlightSearch.Api.Application;
using FlightSearch.Common.Settings;
using FlightSearch.Service.Airports;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightSearch.Api.Airports
{
    public class AirportsController : AbstractController
    {
        private readonly IAirportsService _airportsService;

        public AirportsController(
            IOptions<AppSettings> appSettings,
            IAirportsService airportsService
            ): base(appSettings)
        {
            _airportsService = airportsService;
        }
    
        [HttpGet("all")]
        public async Task<IActionResult> Get()
        {
            var airports = await _airportsService.GetAllAsync();
            return Ok(airports);
        }
    }
}