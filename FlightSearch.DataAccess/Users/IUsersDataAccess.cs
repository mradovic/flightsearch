﻿using FlightSearch.DataAccess.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FlightSearch.DataAccess.Users
{
    public interface IUsersDataAccess: IDataAccess
    {
        Task<List<User>> FindAllAsync();
        Task<int> InsertAsync(User user);
        Task<User> FindByUsernameAsync(string username);
    }
}
