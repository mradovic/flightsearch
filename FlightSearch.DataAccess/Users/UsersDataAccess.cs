﻿using FlightSearch.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FlightSearch.DataAccess.Users
{
    public class UsersDataAccess : IUsersDataAccess
    {
        private readonly IFlightSearchDbContextFactory _factory;

        public UsersDataAccess(IFlightSearchDbContextFactory factory)
        {
            _factory = factory;
        }

        public async Task<List<User>> FindAllAsync()
        {
            using (var context = _factory.Create())
            {
                var users = await context.Users
                    .ToListAsync();

                return users;
            }
        }

        public async Task<User> FindByUsernameAsync(string username)
        {
            using (var context = _factory.Create())
            {
                return await context.Users
                    .FirstOrDefaultAsync(u => u.Username == username);
            }
        }

        public async Task<int> InsertAsync(User user)
        {
            using (var context = _factory.Create())
            {
                await context.Users.AddAsync(user);
                await context.SaveChangesAsync();
                return user.UserId;
            }
        }
    }
}
