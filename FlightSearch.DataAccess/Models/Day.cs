﻿using FlightSearch.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FlightSearch.DataAccess.Models
{
    [Table(name: "Days", Schema = Constants.DatabaseSchemaName)]
    public class Day
    {
        [Key]
        public int DayId { get; set; }

        public string Name { get; set; }

        public List<FlightDay> FlightDays { get; set; }

    }
}
