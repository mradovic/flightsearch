﻿using FlightSearch.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FlightSearch.DataAccess.Models
{
    [Table(name: "FlightDays", Schema = Constants.DatabaseSchemaName)]
    public class FlightDay
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int FlightId { get; set; }

        public Flight Flight { get; set; }

        public int DayId { get; set; }

        public Day Day { get; set; }
    }
}
