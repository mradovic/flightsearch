﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FlightSearch.DataAccess.Migrations
{
    public partial class UpdateFlightsTableAirportId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "OriginAirportId",
                schema: "FlightSearch",
                table: "Flights",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DestinationAirportId",
                schema: "FlightSearch",
                table: "Flights",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "OriginAirportId",
                schema: "FlightSearch",
                table: "Flights",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "DestinationAirportId",
                schema: "FlightSearch",
                table: "Flights",
                nullable: true,
                oldClrType: typeof(int));
        }
    }
}
