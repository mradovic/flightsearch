﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FlightSearch.DataAccess.Migrations
{
    public partial class TemporaryRemoveCountryId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Airports_Countries_CountryId",
                schema: "FlightSearch",
                table: "Airports");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Countries",
                schema: "FlightSearch",
                table: "Countries");

            migrationBuilder.DropIndex(
                name: "IX_Airports_CountryId",
                schema: "FlightSearch",
                table: "Airports");

            migrationBuilder.DropColumn(
                name: "CountryId",
                schema: "FlightSearch",
                table: "Countries");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                schema: "FlightSearch",
                table: "Countries",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<string>(
                name: "CountryName",
                schema: "FlightSearch",
                table: "Airports",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Countries",
                schema: "FlightSearch",
                table: "Countries",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_Airports_CountryName",
                schema: "FlightSearch",
                table: "Airports",
                column: "CountryName");

            migrationBuilder.AddForeignKey(
                name: "FK_Airports_Countries_CountryName",
                schema: "FlightSearch",
                table: "Airports",
                column: "CountryName",
                principalSchema: "FlightSearch",
                principalTable: "Countries",
                principalColumn: "Name",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Airports_Countries_CountryName",
                schema: "FlightSearch",
                table: "Airports");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Countries",
                schema: "FlightSearch",
                table: "Countries");

            migrationBuilder.DropIndex(
                name: "IX_Airports_CountryName",
                schema: "FlightSearch",
                table: "Airports");

            migrationBuilder.DropColumn(
                name: "CountryName",
                schema: "FlightSearch",
                table: "Airports");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                schema: "FlightSearch",
                table: "Countries",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<int>(
                name: "CountryId",
                schema: "FlightSearch",
                table: "Countries",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Countries",
                schema: "FlightSearch",
                table: "Countries",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Airports_CountryId",
                schema: "FlightSearch",
                table: "Airports",
                column: "CountryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Airports_Countries_CountryId",
                schema: "FlightSearch",
                table: "Airports",
                column: "CountryId",
                principalSchema: "FlightSearch",
                principalTable: "Countries",
                principalColumn: "CountryId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
