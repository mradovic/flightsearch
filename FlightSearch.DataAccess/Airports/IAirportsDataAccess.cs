﻿using FlightSearch.DataAccess.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FlightSearch.DataAccess.Airports
{
    public interface IAirportsDataAccess: IDataAccess
    {
        Task<List<Airport>> FindAllAsync();
        Task<Airport> FindById(int id);
    }
}
