﻿using FlightSearch.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Threading;
using System.Threading.Tasks;

namespace FlightSearch.DataAccess
{
    public class FlightSearchDbContext : DbContext, IFlightSearchDbContext
    {
        private readonly IConfiguration _configuration;

        #region DbSets

        public DbSet<Airport> Airports { get; set; }

        public DbSet<Country> Countries { get; set; }

        public DbSet<Day> Days { get; set; }

        public DbSet<Flight> Flights { get; set; }

        public DbSet<FlightDay> FlightDays { get; set; }

        public DbSet<User> Users { get; set; }

        #endregion

        public FlightSearchDbContext() { }

        public FlightSearchDbContext(IConfiguration configuration, DbContextOptions<FlightSearchDbContext> options) : base(options)
        {
            _configuration = configuration;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Airport>().HasIndex(_ => _.Code).IsUnique(true);
            builder.Entity<Flight>()
                    .HasOne(f => f.OriginAirport)
                    .WithMany(f => f.OriginFlights)
                    .HasForeignKey(f => f.OriginAirportId)
                    .OnDelete(DeleteBehavior.Restrict);
            builder.Entity<Flight>()
                    .HasOne(f => f.DestinationAirport)
                    .WithMany(f => f.DestinationFlights)
                    .HasForeignKey(f => f.DestinationAirportId)
                    .OnDelete(DeleteBehavior.Restrict);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // to do: connection string should be retrieved from appsettings
            optionsBuilder.UseSqlServer(@"server=localhost\SQLEXPRESS;database=FlightSearch;Trusted_Connection=True;MultipleActiveResultSets=true");
        }

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return (await base.SaveChangesAsync(true, cancellationToken));
        }
    }
}
