﻿using Microsoft.EntityFrameworkCore.Design;

namespace FlightSearch.DataAccess
{
    public interface IFlightSearchDbContextFactory : IDataAccess, IDesignTimeDbContextFactory<FlightSearchDbContext>
    {
        IFlightSearchDbContext Create();
    }
}
