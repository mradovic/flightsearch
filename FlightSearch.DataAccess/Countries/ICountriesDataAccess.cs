﻿using FlightSearch.DataAccess.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FlightSearch.DataAccess.Countries
{
    public interface ICountriesDataAccess : IDataAccess
    {
        Task<List<Country>> FindAllAsync();
    }
}
