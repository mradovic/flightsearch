﻿using FlightSearch.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FlightSearch.DataAccess.Countries
{
    public class CountriesDataAccess : ICountriesDataAccess
    {
        private readonly IFlightSearchDbContextFactory _factory;

        public CountriesDataAccess(IFlightSearchDbContextFactory factory)
        {
            _factory = factory;
        }

        public async Task<List<Country>> FindAllAsync()
        {
            using (var context = _factory.Create())
            {
                return await context.Countries.ToListAsync();
            }
        }
    }
}
