﻿using FlightSearch.DataAccess.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
namespace FlightSearch.DataAccess.Flights
{
    public class FlightsDataAccess : IFlightsDataAccess
    {
        private readonly IFlightSearchDbContextFactory _factory;

        public FlightsDataAccess(IFlightSearchDbContextFactory factory)
        {
            _factory = factory;
        }

        public async Task AddFlightDaysToFlightAsync(int flightId, List<int> days)
        {
            using (var context = _factory.Create())
            {
                var flight = await context.Flights
                    .FirstOrDefaultAsync(f => f.FlightId == flightId);
                flight.FlightDays = new List<FlightDay>();
                foreach (var item in days)
                {
                    var flightDay = new FlightDay
                    {
                        DayId = item,
                        FlightId = flight.FlightId
                    };
                    flight.FlightDays.Add(flightDay);
                }
                context.Flights.Update(flight);
                await context.SaveChangesAsync();
            }
        }

        public async Task<Flight> EditAsync(int id, Flight flight)
        {
            using (var context = _factory.Create())
            {
                context.Flights.Update(flight);
                await context.SaveChangesAsync();
            }
            return await FindByIdAsync(id);
        }

        public async Task<Flight> FindByCodeAsync(string code)
        {
            using (var context = _factory.Create())
            {
                var flight = await context.Flights
                    .FirstOrDefaultAsync(f => f.Code.Equals(code));
                return flight;
            }
        }

        public async Task<List<Flight>> FindByDestinationAsync(int destinationId)
        {
            using (var context = _factory.Create())
            {
                return await context.Flights
                    .Include(x => x.OriginAirport)
                    .Include(x => x.DestinationAirport)
                    .Include($"{nameof(Flight.FlightDays)}.{nameof(FlightDay.Day)}")
                    .Where(f => f.DestinationAirport.AirportId == destinationId)
                    .ToListAsync();
            }
        }

        public async Task<Flight> FindByIdAsync(int id)
        {
            using (var context = _factory.Create())
            {
                var flight = await context.Flights
                    .Include(f => f.DestinationAirport)
                    .Include(f => f.OriginAirport)
                    .Include($"{nameof(Flight.FlightDays)}.{nameof(FlightDay.Day)}")
                    .FirstOrDefaultAsync(f => f.FlightId == id);
                return flight;
            }
        }

        public async Task<List<Flight>> FindByOriginAsync(int originId)
        {
            using (var context = _factory.Create())
            {
                return await context.Flights
                    .Include(x => x.OriginAirport)
                    .Include(x => x.DestinationAirport)
                    .Include($"{nameof(Flight.FlightDays)}.{nameof(FlightDay.Day)}")
                    .Where(f => f.OriginAirport.AirportId == originId)
                    .ToListAsync();
            }
        }

        public async Task<List<Flight>> FindByOriginAndDestinationAsync(int originId, int destinationId)
        {
            using (var context = _factory.Create())
            {
                return await context.Flights
                    .Include(x => x.DestinationAirport)
                    .Include(x => x.OriginAirport)
                    .Include($"{nameof(Flight.FlightDays)}.{nameof(FlightDay.Day)}")
                    .Where(f => f.OriginAirport.AirportId == originId && f.DestinationAirport.AirportId == destinationId)
                    .ToListAsync();
            }
        }

        public async Task<Flight> InsertAsync(Flight flight)
        {
            using (var context = _factory.Create())
            {
                context.Flights.Add(flight);
                await context.SaveChangesAsync();
            }
            return await FindByIdAsync(flight.FlightId);
        }

        public async Task RemoveFlightDaysAsync(int flightId)
        {
            using (var context = _factory.Create())
            {
                var flightDays = context.FlightDays
                    .Where(fd => fd.FlightId == flightId);
                context.FlightDays.RemoveRange(flightDays);
                await context.SaveChangesAsync();
            }
        }

        public async Task DeleteFlightAsync(int id)
        {
            using (var context = _factory.Create())
            {
                var flight = await context.Flights.FirstOrDefaultAsync(f => f.FlightId == id);
                context.Flights.Remove(flight);
                await context.SaveChangesAsync();
            }
        }

        public async Task<List<Flight>> FindByOriginOrDestinationAsync(int originId, int destinationId)
        {
            using (var context = _factory.Create())
            {
                return await context.Flights
                    .Include(x => x.DestinationAirport)
                    .Include(x => x.OriginAirport)
                    .Include($"{nameof(Flight.FlightDays)}.{nameof(FlightDay.Day)}")
                    .Where(f => f.OriginAirport.AirportId == originId || f.DestinationAirport.AirportId == destinationId)
                    .Distinct()
                    .ToListAsync();
            }
        }
    }
}
