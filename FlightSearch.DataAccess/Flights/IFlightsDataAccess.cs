﻿using FlightSearch.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FlightSearch.DataAccess.Flights
{
    public interface IFlightsDataAccess: IDataAccess
    {
        Task<List<Flight>> FindByOriginAndDestinationAsync(int originId, int destinationId);
        Task DeleteFlightAsync(int id);
        Task<Flight> FindByIdAsync(int id);
        Task<Flight> EditAsync(int id, Flight flight);
        Task<Flight> FindByCodeAsync(string code);
        Task<Flight> InsertAsync(Flight flight);
        Task RemoveFlightDaysAsync(int id);
        Task AddFlightDaysToFlightAsync(int flightId, List<int> days);
        Task<List<Flight>> FindByOriginAsync(int originId);
        Task<List<Flight>> FindByDestinationAsync(int destinationId);
        Task<List<Flight>> FindByOriginOrDestinationAsync(int originId, int destinationId);
    }
}
