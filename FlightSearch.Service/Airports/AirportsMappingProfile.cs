﻿using AutoMapper;
using FlightSearch.DataAccess.Models;

namespace FlightSearch.Service.Airports
{
    class AirportsMappingProfile:Profile
    {
        public AirportsMappingProfile()
        {
            CreateMap<Airport, AirportDomain>()
                .ForMember(dst => dst.Country, opt => opt.MapFrom(src => src.Country.Name))
                .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.AirportId));
            
        }

    }
}
