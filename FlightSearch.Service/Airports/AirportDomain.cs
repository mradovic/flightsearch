﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FlightSearch.Service.Airports
{
    public class AirportDomain
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string City { get; set; }

        public string Description { get; set; }

        public string Country { get; set; }
    }
}
