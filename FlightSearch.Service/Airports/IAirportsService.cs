﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace FlightSearch.Service.Airports
{
    public interface IAirportsService: IService
    {
        Task<List<AirportDomain>> GetAllAsync();
    }
}
