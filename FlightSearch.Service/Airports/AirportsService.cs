﻿using System;
using System.Collections.Generic;
using FlightSearch.DataAccess.Airports;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace FlightSearch.Service.Airports
{
    public class AirportsService: IAirportsService
    {
        private readonly IAirportsDataAccess _airportsDataAccess;
        private readonly IMapper _mapper;

        public AirportsService(IAirportsDataAccess airportsDataAccess, IMapper mapper)
        {
            _airportsDataAccess = airportsDataAccess;
            _mapper = mapper;
        }


        public async Task<List<AirportDomain>> GetAllAsync()
        {
            var airports = await _airportsDataAccess.FindAllAsync();
            var result = _mapper.Map<List<AirportDomain>>(airports);
            return result;
        }
    }
}
