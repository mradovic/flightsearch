﻿namespace FlightSearch.Service.Countries
{
    public class CountryDomain
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
