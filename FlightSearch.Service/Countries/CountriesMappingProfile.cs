﻿using AutoMapper;
using FlightSearch.DataAccess.Models;

namespace FlightSearch.Service.Countries
{
    public class CountriesMappingProfile : Profile
    {
        public CountriesMappingProfile()
        {
            CreateMap<Country, CountryDomain>()
                .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.CountryId));
        }
    }
}
