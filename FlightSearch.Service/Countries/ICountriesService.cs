﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace FlightSearch.Service.Countries
{
    public interface ICountriesService : IService
    {
        Task<List<CountryDomain>> GetAllAsync();
    }
}
