﻿
namespace FlightSearch.Service.Flights
{
    public class FeaturedFlight
    {
        public ConnectingFlightsDomain FlightCombination { get; set; }

        public int Day { get; set; }
    }
}
