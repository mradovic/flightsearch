﻿
namespace FlightSearch.Service.Flights
{
    public class ConnectingFlightsDomain
    {
        public FlightUpdateDomain OriginFlight { get; set; }

        public FlightUpdateDomain DestinationFlight { get; set; }
    }
}
