﻿using FlightSearch.Service.FlightDays;
using System.Collections.Generic;

namespace FlightSearch.Service.Flights
{
    public class FlightUpdateDomain
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public int? OriginAirportId { get; set; }

        public string OriginCity { get; set; }

        public int? DestinationAirportId { get; set; }

        public string DestinationCity { get; set; }

        public string DepartureTime { get; set; }

        public string LandingTime { get; set; }

        public string Agency { get; set; }

        public int? Rows { get; set; }

        public int? Columns { get; set; }

        public int? Price { get; set; }

        public List<FlightDayDomain> FlightDays { get; set; }
    }
}
