﻿
namespace FlightSearch.Service.Flights
{
    public struct IntersectFlights
    {
        public int OriginId { get; set; }
        public int DestinationId { get; set; }
        public int Day { get; set; }
        public int TimeDifference { get; set; }
    }
}
