﻿using AutoMapper;
using FlightSearch.DataAccess.Models;
using FlightSearch.Service.FlightDays;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace FlightSearch.Service.Flights
{
    public class FlightsMappingProfile: Profile
    {
        public FlightsMappingProfile()
        {
            CreateMap<Flight, FlightDomain>()
                .ForMember(dst => dst.OriginCity, opt => opt.MapFrom(src => src.OriginAirport.City))
                .ForMember(dst => dst.DestinationCity, opt => opt.MapFrom(src => src.DestinationAirport.City))
                .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.FlightId))
                .ForMember(dst => dst.Days, opt => opt.MapFrom(src => src.FlightDays)).ReverseMap();
            CreateMap<Flight, FlightUpdateDomain>()
                .ForMember(dst => dst.OriginCity, opt => opt.MapFrom(src => src.OriginAirport.City))
                .ForMember(dst => dst.DestinationCity, opt => opt.MapFrom(src => src.DestinationAirport.City))
                .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.FlightId))
                .ForMember(dst => dst.FlightDays, opt => opt.MapFrom(src => src.FlightDays)).ReverseMap();
            CreateMap<Flight, FlightDomain>()
                .ForMember(dst => dst.OriginCity, opt => opt.MapFrom(src => src.OriginAirport.City))
                .ForMember(dst => dst.DestinationCity, opt => opt.MapFrom(src => src.DestinationAirport.City))
                .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.FlightId))
                .ForMember(dst => dst.Days, opt => opt.MapFrom(src => src.FlightDays.Select(x => x.DayId)));
        }
    }
}
