﻿using System.Collections.Generic;
using System.Threading.Tasks;
namespace FlightSearch.Service.Flights
{
    public interface IFlightsService: IService
    {
        Task<FlightsCombinationDomain> GetByOriginAndDestinationAsync(int originId, int destinationId);
        Task RemoveAsync(int id);
        Task<FlightDomain> GetByIdAsync(int id);
        Task<FlightUpdateDomain> EditAsync(int id, FlightDomain flightDto);
        Task<FlightUpdateDomain> CreateAsync(FlightDomain flightDto);
        Task<List<ConnectingFlightsDomain>> GetConnectingFlightsAsync(int originId, int destinationId);
    }
}
