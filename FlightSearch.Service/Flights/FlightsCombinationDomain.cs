﻿using System.Collections.Generic;

namespace FlightSearch.Service.Flights
{
    public class FlightsCombinationDomain
    {
        public List<FlightUpdateDomain> DirectFlights { get; set; }

        public List<ConnectingFlightsDomain> ConnectingFlights { get; set; }

        public FeaturedFlight FeaturedFlight { get; set; }
    }
}
