﻿using AutoMapper;
using FlightSearch.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlightSearch.Service.Days
{
    class DayMappingProfile: Profile
    {
        public DayMappingProfile()
        {
            CreateMap<Day, DayDomain>().ReverseMap();
        }
    }
}
