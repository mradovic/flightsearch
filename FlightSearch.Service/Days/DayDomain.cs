﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightSearch.Service.Days
{
    public class DayDomain
    {
        public int DayId { get; set; }
        public string Name { get; set; }
    }
}
