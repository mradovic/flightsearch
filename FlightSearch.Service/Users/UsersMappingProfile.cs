﻿using AutoMapper;
using FlightSearch.DataAccess.Models;

namespace FlightSearch.Service.Users
{
    public class UsersMappingProfile : Profile
    {
        public UsersMappingProfile()
        {
            CreateMap<User, UserDomain>();
            CreateMap<ReqisterRequestDomain, User>(); 
            CreateMap<User, RegisterResponseDomain>();
        }
    }
}
