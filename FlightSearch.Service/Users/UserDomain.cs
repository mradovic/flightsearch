﻿namespace FlightSearch.Service.Users
{
    public class UserDomain
    {
        public int UserId { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }
    }
}
