﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace FlightSearch.Service.Users
{
    public interface IUsersService: IService
    {
        Task<List<UserDomain>> GetAllAsync();
        Task<RegisterResponseDomain> RegisterAsync(ReqisterRequestDomain registerRequest);
    }
}
