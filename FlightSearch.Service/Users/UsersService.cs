﻿using AutoMapper;
using FlightSearch.Common.Extensions;
using FlightSearch.Common.Settings;
using FlightSearch.DataAccess.Models;
using FlightSearch.DataAccess.Users;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FlightSearch.Service.Users
{
    public class UsersService : IUsersService
    {
        private readonly IUsersDataAccess _usersDataAccess;
        private readonly IMapper _mapper;
        protected AppSettings _appSettings { get; private set; }

        public UsersService(IUsersDataAccess usersDataAccess, IMapper mapper, IOptions<AppSettings> appSettings)
        {
            _usersDataAccess = usersDataAccess;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        public async Task<List<UserDomain>> GetAllAsync()
        {
            var users = await _usersDataAccess.FindAllAsync();
            var result = _mapper.Map<List<UserDomain>>(users);
            return result;
        }

        public async Task<RegisterResponseDomain> RegisterAsync(ReqisterRequestDomain registerRequest)
        {
            if (await _usersDataAccess.FindByUsernameAsync(registerRequest.Username) != null)
            {
                throw new NotFoundException(_appSettings.FoundUser);
            }
            var user = _mapper.Map<User>(registerRequest);
            var userId = await _usersDataAccess.InsertAsync(user);
            user.UserId = userId;
            var response = _mapper.Map<RegisterResponseDomain>(user);
            return response;
        }
    }
}
