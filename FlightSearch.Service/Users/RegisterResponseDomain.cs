﻿namespace FlightSearch.Service.Users
{
    public class RegisterResponseDomain
    {
        public int UserId { get; set; }

        public string Username { get; set; }
    }
}
