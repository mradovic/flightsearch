﻿using AutoMapper;
using FlightSearch.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlightSearch.Service.FlightDays
{
    class FlightDaysMappingProfile: Profile
    {
        public FlightDaysMappingProfile()
        {
            CreateMap<FlightDay, FlightDayDomain>().ReverseMap();
        }
    }
}
