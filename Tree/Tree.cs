﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Tree
{
    public class Tree
    {
        public Node root;

        public Tree()
        {
            root = new Node('0');
        }

        public void CreateTreeRec(List<Airport> airports)
        {
            foreach (var airport in airports)
            {
                CreateNodeRec(root, airport, 0);
            }
        }

        public void CreateNodeRec(Node parent, Airport airport, int level)
        {
            Node foundChild = parent.children.FirstOrDefault(x => x.value == airport.Code[level]);
            if (foundChild == null)
            {
                if (level == airport.Code.Length - 1)
                {
                    parent.AddChild(airport.Code[level], airport.Id);
                }
                else
                {
                    Node newChild = parent.AddChild(airport.Code[level]);
                    level++;
                    CreateNodeRec(newChild, airport, level);
                }
            }
            else
            {
                if (level == airport.Code.Length - 1)
                {
                    parent.AddChild(airport.Code[level], airport.Id);
                }
                else
                {
                    level++;
                    CreateNodeRec(foundChild, airport, level);
                }
            }
        }

        public void CreateTree(List<Airport> airports)
        {

            foreach (var airport in airports)
            {
                Node foundFirstDescendant = root.children.FirstOrDefault(n => n.value == airport.Code[0]);

                //first order node not exist
                if (foundFirstDescendant == null)
                {
                    Node firstOrderNode = root.AddChild(airport.Code[0]);
                    Node scndOrderNode = firstOrderNode.AddChild(airport.Code[1]);
                    scndOrderNode.AddChild(airport.Code[2], airport.Id);
                }
                //first order node exists
                else
                {
                    Node foundScndDescendant = foundFirstDescendant.children.FirstOrDefault(x => x.value == airport.Code[1]);

                    //second order node not exist
                    if (foundScndDescendant == null)
                    {
                        Node scndOrderNode = foundFirstDescendant.AddChild(airport.Code[1]);
                        scndOrderNode.AddChild(airport.Code[2], airport.Id);
                    }
                    //second order node exists
                    else
                    {
                        Node foundThirdDescendant = foundFirstDescendant.children.FirstOrDefault(x => x.value == airport.Code[2]);
                        if (foundThirdDescendant == null)
                            foundScndDescendant.AddChild(airport.Code[2], airport.Id);
                    }
                }
            }
        }

        public int SearchByCodeRec(string code)
        {
            string foundId = SearchNodeRec(root, level:0, code);
            if (int.TryParse(foundId, out int id))
            {
                return id;
            }
            else
            {
                return -1;
            }
        }

        public string SearchNodeRec(Node parent, int level, string code)
        {

            string nodeValue = "";
          
            if (parent.children.Count > 0)
            {
                
                foreach (var child in parent.children)
                {
                    if (level > code.Length - 1)
                    {
                        break;
                    }

                    if (child.value == code[level])
                    {
                        level++;

                        //node is last order descendant
                        if (level == code.Length)
                        {
                            if (child is LeafNode)
                            {
                                LeafNode leafNode = (LeafNode)child;
                                nodeValue += leafNode.id.ToString();
                            }
                        }
                        return nodeValue + SearchNodeRec(child, level, code);
                    }
                }
            }

            return nodeValue;
        }

        public void SearchByCode(string code)
        {
            bool flag = false;
            string airportCode = "";
            int airportId = 0;

            foreach (var firstOrderChild in root.children)
            {
                //first order node exist
                if (firstOrderChild.value == code[0])
                {
                    foreach (var scndOrderChild in firstOrderChild.children)
                    {
                        //second order node exist
                        if (scndOrderChild.value == code[1])
                        {
                            foreach (var thirdOrderChild in scndOrderChild.children)
                            {
                                //third order node exist
                                if (thirdOrderChild.value == code[2])
                                {
                                    LeafNode leafNode = (LeafNode)thirdOrderChild;
                                    flag = true;
                                    airportId = leafNode.id;
                                    airportCode = firstOrderChild.value.ToString() + scndOrderChild.value.ToString() + thirdOrderChild.value.ToString();
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            //airport code exist
            if (flag)
            {
                Console.WriteLine("Airport id: {0}, airport code: {1}.", airportId, airportCode);
            }
            //airport code not exist
            else
            {
                Console.WriteLine("Airport wiwht code: {0} is not found.", code);
            }
        }
    }
}

