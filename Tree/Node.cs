﻿using System.Collections.Generic;
using System.Linq;

namespace Tree
{
    public class Node
    {
        public char value;

        public List<Node> children;

        public Node(char value)
        {
            this.value = value;
            children = new List<Node>();
        }

        public Node AddChild(char code)
        {
            Node newNode = new Node(code);
            children.Add(newNode);
            children = children.OrderBy(x => x.value).ToList();
            return newNode;
        }

        public void AddChild(char code, int id)
        {
            LeafNode newNode = new LeafNode(code, id);
            children.Add(newNode);
            children = children.OrderBy(x => x.value).ToList();
        }
    }
}
