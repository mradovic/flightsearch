﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Tree
{
    class Program
    {

        static void Main(string[] args)
        {
            //List<string> codes = new List<string> { "AAA", "ABZ", "ACZ", "CBZ", "ABM" };
            Dictionary<string, int> codes = new Dictionary<string, int> { { "AAA", 1 }, { "ABZ", 2 }, { "ACZ", 4 }, { "CBZ", 7 }, { "ABM", 8 } };

            var airports = new List<Airport>() { new Airport(1, "AAA"), new Airport(2, "ABZ"), new Airport(4, "ACZ"), new Airport(7, "CBZ"), new Airport( 8, "ABM") };
           
            Tree tree = new Tree();

            tree.CreateTree(airports);

            foreach (var first in tree.children)
            {
                Console.WriteLine("first " + first.value);

                foreach(var second in first.children)
                {
                    Console.WriteLine("second " + second.value);

                    foreach (var third in second.children)
                    {
                        Console.WriteLine("third " + third.value + " id: " + third.id);
                    }
                }
            }
        }

        public class Node
        {
            public string value;
            public int id;

            public List<Node> children = new List<Node>();

            public Node(string value)
            {
                this.value = value;
            }

            public Node(string value, int id)
            {
                this.value = value;
                this.id = id;
            }

            public void AddChild(Node node)
            {
                if (children.FirstOrDefault(n => n.value == node.value) == null)
                {
                    children.Add(node);
                }
            }
        }

        public class Tree
        {
            private readonly Node root;
            public List<Node> children;

            public Tree()
            {
                root = new Node("");
                children = new List<Node>();
            }

            public void CreateTree(List<Airport> airports)
            {

                foreach (var item in airports)
                {

                    Node temp = children.FirstOrDefault(n => n.value == Char.ToString(item.Code[0]));

                    //first order node not exist
                    if (temp == null)
                    {
                        Node firstOrderNode = new Node(Char.ToString(item.Code[0]));
                        children.Add(firstOrderNode);

                        Node scndOrderNode = new Node(Char.ToString(item.Code[1]));
                        firstOrderNode.children.Add(scndOrderNode);

                        Node thirdOrderNode = new Node(Char.ToString(item.Code[2]), item.Id);
                        scndOrderNode.children.Add(thirdOrderNode);

                    }

                    //first order node exists
                    else
                    {
                        Node tempScnd = temp.children.FirstOrDefault(x => x.value == Char.ToString(item.Code[1]));

                        //second order node does not exist
                        if (tempScnd == null)
                        {
                            Node scndOrderNode = new Node(Char.ToString(item.Code[1]));
                            temp.children.Add(scndOrderNode);

                            Node thirdOrderNode = new Node(Char.ToString(item.Code[2]), item.Id);
                            scndOrderNode.children.Add(thirdOrderNode);
                        }

                        //second order node does exist
                        else
                        {

                            Node tempThird = temp.children.FirstOrDefault(x => x.value == Char.ToString(item.Code[2]));
                            if (tempThird == null)
                            {
                                Node thirdOrderNode = new Node(Char.ToString(item.Code[2]), item.Id);
                                tempScnd.children.Add(thirdOrderNode);
                            }
                        }
                    }
                }
            }


            public void CreateTree(Dictionary<string, int> list)
            {

                foreach (var item in list)
                {

                    Node temp = children.FirstOrDefault(n => n.value == Char.ToString(item.Key[0]));

                    //first order node not exist
                    if (temp == null)
                    {
                        Node firstOrderNode = new Node(Char.ToString(item.Key[0]));
                        children.Add(firstOrderNode);

                        Node scndOrderNode = new Node(Char.ToString(item.Key[1]));
                        firstOrderNode.children.Add(scndOrderNode);

                        Node thirdOrderNode = new Node(Char.ToString(item.Key[2]), item.Value);
                        scndOrderNode.children.Add(thirdOrderNode);

                    }

                    //first order node exists
                    else
                    {
                        Node tempScnd = temp.children.FirstOrDefault(x => x.value == Char.ToString(item.Key[1]));

                        //second order node does not exist
                        if (tempScnd == null)
                        {
                            Node scndOrderNode = new Node(Char.ToString(item.Key[1]));
                            temp.children.Add(scndOrderNode);

                            Node thirdOrderNode = new Node(Char.ToString(item.Key[2]), item.Value);
                            scndOrderNode.children.Add(thirdOrderNode);
                        }

                        //second order node does exist
                        else
                        {

                            Node tempThird = temp.children.FirstOrDefault(x => x.value == Char.ToString(item.Key[2]));
                            if (tempThird == null)
                            {
                                Node thirdOrderNode = new Node(Char.ToString(item.Key[2]), item.Value);
                                tempScnd.children.Add(thirdOrderNode);
                            }
                        }

                    }
                }
            }

           
        }
    }
}
