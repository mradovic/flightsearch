﻿namespace Tree
{
    public class LeafNode : Node
    {
        public int id;

        public LeafNode(char value, int id): base(value)
        {
            this.value = value;
            this.id = id;
        }
    }
}
