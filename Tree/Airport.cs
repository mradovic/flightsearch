﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tree
{
    public class Airport
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public Airport( int id, string code)
        {
            Id = id;
            Code = code;
        }

        public Airport()
        {
        }
    }
}
